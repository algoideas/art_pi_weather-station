#ifndef __HTTP_CLIENT_H_
#define __HTTP_CLIENT_H_

#include <rtthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct http_weather_info_s {
    char date[8];
    char text_day[8];
    char code_day[8];
    char text_night[8];
    char high[8];
    char low[8];
    char rainfall[8];
    char humidity[8];
    char wind_direction[16];
    char wind_speed[8];
}http_weather_info_t;

int http_weather_collect(void);
http_weather_info_t *http_weather_info(void);

#ifdef  __cplusplus
}
#endif
#endif

