/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-21     Algo         the first version
 */

#include <rtthread.h>
#include <fal.h>
#include <dfs_posix.h>

#define DBG_TAG "datastg"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#include <datastorage.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DATA_STORAGE_ON

int datastorage_save(char *filename, data_info_t info)
{
#ifdef DATA_STORAGE_ON
    struct statfs buffer;
    unsigned char data[64];
    int fd = 0;

    if (rt_device_find("sd0") != RT_NULL)
    {
        if ((dfs_statfs("/sdcard",&buffer) == RT_EOK) | (dfs_mount("sd0", "/sdcard", "elm", 0, 0) == RT_EOK))
        {
            fd = open(filename, O_CREAT | O_RDWR | O_APPEND);
            if (fd >= 0) {

                /* 温湿度数据存储格式 ：年-月-日;时:分:秒;温度数据 (单位:摄氏度);湿度数据  */
                memset(data, 0x0, 64);
                snprintf(data, 64, "%d-%02d-%02d;%02d:%02d:%02d;%d;%d\n", info.yy, info.mm, info.dd,
                        info.hh, info.min, info.sec, info.temp, info.hum);

                write(fd, data, sizeof(data));
                close(fd);
            }
        }
    }
#endif

    return 0;
}

#ifdef  __cplusplus
}
#endif
