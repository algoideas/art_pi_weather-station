#ifndef __DATASTG_H
#define __DATASTG_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct data_info {
    rt_int32_t dd, mm, yy;
    rt_int32_t hh, min, sec;
    rt_int32_t temp;
    rt_int32_t hum;
}data_info_t;

int datastorage_save(char *filename, data_info_t info);

#ifdef  __cplusplus
}
#endif
#endif  /*__DATASTG_H */
 
