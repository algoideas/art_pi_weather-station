#ifndef __WIFIMNG_H
#define __WIFIMNG_H

#define WIFI_DEVICE_NAME "w0"

int wifimng_init(void);
int wifi_is_ready(void);
char *wifi_get_ip(void);
int wifi_connect(char *conn_str);
char *wifi_status_get(void);

#endif /*__WIFIMNG_H */
