/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#include <rtthread.h>
#include <rtdevice.h>

#include "appcomm.h"
#include "eventhub.h"
#include "keymng.h"

#define DBG_TAG "KEYMNG"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>


/** key information */
typedef struct _key_info
{
    key_state_t state;
    rt_bool_t short_press_done;
    rt_bool_t short_press;
    rt_bool_t long_press_done;
    rt_bool_t long_press;
    rt_bool_t double_press_done;
    rt_bool_t double_press;
    rt_uint32_t timtick;
    rt_uint32_t double_timtick;
} key_info_t;

static key_info_t key_info;
static rt_bool_t enable_key = RT_TRUE;

/**
 * @brief  key event check for short click and hold mode
 * @param[in]enKeyType : key type index
 * @param[in/out]info : keymng information
 * @return NA
 */
static void key_hold_check(key_info_t *info)
{
    eventhub_event_t event;

    /* Time tick count */
    info->timtick ++;
    info->double_timtick ++;

    if ((KEY_STATE_DOWN == info->state))
    {
        if (RT_FALSE == info->short_press_done)
        {
            info->short_press_done = RT_TRUE;
            info->timtick = 0;
        }
        else if (RT_TRUE == info->short_press_done)
        {
            if (info->timtick > (KEY_LONG_PRESS_TIME / KEY_CHECK_INTERVAL)) 
            {
                info->short_press_done = RT_FALSE;
                if (RT_TRUE != info->long_press)
                {
                    info->long_press = RT_TRUE;
                    LOG_D("Key Hold [%d]", KEY_EVT_HOLD);
                    event.eventId =  KEY_EVT_HOLD;
                    eventhub_publish(&event);
                }
            }
        }  
    }

    if ((KEY_STATE_UP == info->state))
    {
        if (RT_TRUE == info->short_press_done)
        {
            info->short_press_done = RT_FALSE;
            if (RT_FALSE == info->double_press_done)
            {
                info->double_press_done = RT_TRUE;
                info->double_timtick = 0;
            }
            else
            {
                if (info->double_timtick <  (KEY_DOUBLE_PRESS_TIME / KEY_CHECK_INTERVAL)) 
                {
                    info->double_press_done = RT_FALSE;
                    if (RT_TRUE == info->long_press)
                    {
                        info->long_press = RT_FALSE;
                    }
                
                    info->double_press = RT_TRUE;
                    LOG_D("Key Double [%d]", KEY_EVT_DOUBLE);
                    event.eventId =  KEY_EVT_DOUBLE;
                    eventhub_publish(&event);
                }
            }
        }
        else if (RT_TRUE == info->double_press_done)
        {
            if (info->double_timtick > (KEY_SHORT_PRESS_TIME / KEY_CHECK_INTERVAL))
            {
                info->double_press_done = RT_FALSE;
                
                if (RT_TRUE == info->long_press)
                {
                    info->long_press = RT_FALSE;
                    info->short_press = RT_TRUE;
                }
                else
                {
                    info->short_press = RT_TRUE;
                    LOG_D("Key Down [%d]", KEY_EVT_DOWN);
                    event.eventId = KEY_EVT_DOWN;
                    eventhub_publish(&event);
                }
            }  
        }
    }  

    if ((KEY_STATE_UP == info->state))
    {
        if ((RT_TRUE == info->short_press) || (RT_TRUE == info->double_press))
        {
            info->short_press  = RT_FALSE;
            info->double_press = RT_FALSE;
            LOG_D("Key Up [%d]", KEY_EVT_UP);
            event.eventId = KEY_EVT_UP;
            eventhub_publish(&event);
        }
    }

    return ;
}

/**
 * @brief  key irq callback
 * @param[in] args : param
 * @return NA
 */
static void key_irq_callback(void *args)
{
    key_info.state = rt_pin_read(KEY_PIN);
}

static void key_entry(void *parameter)
{
    while (1)
    {
        if (!enable_key)
        {
            rt_thread_delay(RT_TICK_PER_SECOND);
            continue;
        }

        key_hold_check(&key_info);

        rt_thread_delay(RT_TICK_PER_SECOND / 100);  
    }

    return ;
}


int keymng_init(void)
{
    rt_thread_t tid;

    /* default */
    enable_key = RT_TRUE;
    key_info.state = KEY_STATE_UP;

    rt_pin_mode(KEY_PIN, PIN_MODE_INPUT);
    rt_pin_attach_irq(KEY_PIN, PIN_IRQ_MODE_RISING_FALLING, key_irq_callback, &enable_key);
    rt_pin_irq_enable(KEY_PIN, RT_TRUE);

    tid = rt_thread_create("key_init", key_entry,
            RT_NULL, 
            1024,
            17,
            10);
    if (tid != RT_NULL)
    {
        rt_thread_startup(tid);
    }
    else
    {
        LOG_E("rt_thread_create key_init thread failed!");
        return RT_ERROR;
    }

    return 0;
}

int keymng_enable(rt_bool_t enable)
{
    enable_key = enable;
    return RT_EOK;
}


