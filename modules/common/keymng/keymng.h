/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author          Notes
 * 2020-11-01     Algo            the first version.
 * Note:
 * User Key for ART-Pi
 */

#ifndef __KEYMNG_H
#define __KEYMNG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <drv_common.h>

/* define user key wiring pin: PH4 */
#define KEY_PIN                 GET_PIN(H, 4)

/** key check interval, unit ms */
#define KEY_CHECK_INTERVAL      (10)

/** key long click event period, check times */
#define KEY_LONGCLICK_CNT       (40)

/** key long press times , unit ms */
#define KEY_LONG_PRESS_TIME     (400)
/** key short press times */
#define KEY_SHORT_PRESS_TIME    (40)
/** key long press times */
#define KEY_DOUBLE_PRESS_TIME   (40)

/* key status */
enum key_state
{
    KEY_STATE_DOWN = 0x0, KEY_STATE_UP, KEY_STATE_MAX,
};
typedef enum key_state key_state_t;

/** key event id */
#define KEY_EVT_DOWN    APPCOMM_EVENT_ID(APP_MOD_KEY, 0) /**<key down for hold mode or short click mode */
#define KEY_EVT_UP      APPCOMM_EVENT_ID(APP_MOD_KEY, 1) /**<key up for hold mode or short click mode */
#define KEY_EVT_HOLD    APPCOMM_EVENT_ID(APP_MOD_KEY, 2) /**<hold mode for click mode */
#define KEY_EVT_DOUBLE  APPCOMM_EVENT_ID(APP_MOD_KEY, 3) /**<double mode for click mode */
#define KEY_EVT_MAX     APPCOMM_EVENT_ID(APP_MOD_KEY, 4)

/*
 * key init
 */
int keymng_init(void);

/*
 * key enable or disable
 */
int keymng_enable(rt_bool_t enable);

#ifdef __cplusplus
}
#endif

#endif  /*__KEY_H */
