/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#ifndef MODULES_UTILS_EVENTHUB_EVENTHUB_H
#define MODULES_UTILS_EVENTHUB_EVENTHUB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rtthread.h>
#include <rtdef.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

#define HI_EVENTHUB_PAYLOAD_LEN        128
#define HI_EVENTHUB_MAX_EVENT_CNT      32
#define HI_EVENTHUB_MAX_SUBSCRIBER_CNT 32

typedef rt_uint32_t event_id_t;

typedef struct eventhub_event {
    event_id_t eventId;
    rt_int32_t arg1;
    rt_int32_t arg2;
    rt_int32_t result;
    rt_uint64_t createTime;
    rt_int8_t payload[HI_EVENTHUB_PAYLOAD_LEN];
}eventhub_event_t;

typedef rt_int32_t (*eventhub_subcriber_callback_t)(eventhub_event_t *event, void *argv);

typedef struct {
    eventhub_subcriber_callback_t subscriberCb;
    void *argv;
}eventhub_subscriber_t;

rt_int32_t eventhub_init(void);
rt_int32_t eventhub_deinit(void);
rt_int32_t eventhub_register(event_id_t eventId);
rt_int32_t eventhub_unregister(event_id_t eventId);
rt_int32_t eventhub_publish(eventhub_event_t *event);
rt_int32_t eventhub_create_subscriber(eventhub_subscriber_t *subscriber, rt_uint32_t *subscriberHdl);
rt_int32_t eventhub_destroy_subscriber(rt_uint32_t subscriberHdl);
rt_int32_t eventhub_subscribe(rt_uint32_t subscriberHdl, event_id_t eventId);
rt_int32_t eventhub_unsubscribe(rt_uint32_t subscriberHdl, event_id_t eventId);
rt_int32_t eventhub_dump_event_and_subscriberlist(void);

/* new function */
typedef void *(*eventhub_event_trigger_callback_t)(void *data);

typedef struct {
    eventhub_event_trigger_callback_t eventTriggerCb;
    void *priv;
}eventhub_event_trigger_cfg_t;

rt_int32_t eventhub_create_event_trigger(rt_uint8_t *triggerHdl, eventhub_event_trigger_cfg_t *eventTriggerCfg);
rt_int32_t eventhub_trigger_event(rt_uint8_t triggerHdl);
rt_int32_t eventhub_destroy_event_trigger(rt_uint8_t triggerHdl);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif
#endif
