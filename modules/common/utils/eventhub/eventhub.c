/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#include "eventhub.h"
#include "eventhub_trigger.h"
#include "appcomm.h"

#define DBG_TAG "EVENTHUB"

#define EVENTHUB_DEBUG
#ifdef EVENTHUB_DEBUG
#define DBG_LVL               DBG_LOG
#else
#define DBG_LVL               DBG_ERROR
#endif
#include <rtdbg.h>


#define EVENTHUB_MAX_FUNC_PER_EVENT    4
#define EVENTHUB_MAX_EVENT             255
#define EVENTHUB_MAX_SUBSCRIBERS       255
#define EVENTHUB_INVALID_SUBSCRIBER_ID 0xFF
#define EVENTHUB_MAX_QUEUE_SIZE        10
#define EVENTHUB_LOOP_INTERVAL_MS      10
#define EVENTHUB_TASK_STACK_SIZE       0xc00
#define EVENTHUB_TASK_PRIORITY         20

typedef struct {
    void *argv;
    eventhub_subcriber_callback_t subscriberCb;
} EVENTHUB_SubscriberNode;

typedef struct {
    rt_uint32_t eventId;
    rt_uint8_t subscriberIdx[EVENTHUB_MAX_FUNC_PER_EVENT]; /* store the subscriber index only */
} EVENTHUB_EventNode;

typedef struct {
    rt_uint8_t maxEventCnt;
    rt_uint8_t maxSubscriberCnt;
    rt_uint8_t isStarted;
    rt_uint8_t reserved;
#ifdef EVENTHUB_SUPPORT_QUEUE
    rt_uint32_t eventQueue;
    rt_uint32_t eventProcThread;
#endif
    pthread_mutex_t mutex;
    EVENTHUB_SubscriberNode subscribeList[HI_EVENTHUB_MAX_SUBSCRIBER_CNT];
    EVENTHUB_EventNode eventList[HI_EVENTHUB_MAX_EVENT_CNT];
} EVENTHUB_Context;

static EVENTHUB_Context g_ctx = {
    .maxEventCnt = HI_EVENTHUB_MAX_EVENT_CNT,
    .maxSubscriberCnt = HI_EVENTHUB_MAX_SUBSCRIBER_CNT,
    .isStarted = RT_FALSE,
    .reserved = APPCOMM_U8_INVALID_VAL,
#ifdef EVENTHUB_SUPPORT_QUEUE
    .eventQueue = APPCOMM_U32_INVALID_VAL,
    .eventProcThread = APPCOMM_U32_INVALID_VAL,
#endif
    .mutex = APPCOMM_U32_INVALID_VAL,
};

static inline EVENTHUB_Context *GetEventHubCtx(void)
{
    return &g_ctx;
}

static EVENTHUB_EventNode *EVENTHUB_FindEventNodeById(event_id_t eventId)
{
    rt_uint32_t idx;
    EVENTHUB_EventNode *eventNode = RT_NULL;

    for (idx = 0; idx < g_ctx.maxEventCnt; idx++) {
        eventNode = &g_ctx.eventList[idx];
        if (eventNode->eventId == eventId) {
            return eventNode;
        }
    }

    return RT_NULL;
}

static EVENTHUB_EventNode *EVENTHUB_FindUsedEventNode(event_id_t eventId)
{
    return EVENTHUB_FindEventNodeById(eventId);
}

static EVENTHUB_EventNode *EVENTHUB_FindUnusedEventNode(void)
{
    return EVENTHUB_FindEventNodeById(APPCOMM_U32_INVALID_VAL);
}

static EVENTHUB_SubscriberNode *EVENTHUB_FindUsedSubScriberNodeByHandle(rt_uint32_t subcriberHdl)
{
    if ((rt_uint8_t)subcriberHdl < g_ctx.maxSubscriberCnt) {
        return &g_ctx.subscribeList[subcriberHdl];
    } else {
        LOG_E("err:subcriberHdl(%u) shoule small than g_ctx.maxSubscriberCnt(%d).", subcriberHdl,
              g_ctx.maxSubscriberCnt);
    }

    return RT_NULL;
}

static EVENTHUB_SubscriberNode *EVENTHUB_FindUsedSubScriberNodeByAddr(eventhub_subcriber_callback_t subcriberCb)
{
    for (rt_uint32_t idx = 0; idx < g_ctx.maxSubscriberCnt; idx++) {
        if (g_ctx.subscribeList[idx].subscriberCb == subcriberCb) {
            return &g_ctx.subscribeList[idx];
        }
    }

    return RT_NULL;
}

static EVENTHUB_SubscriberNode *EVENTHUB_FindUnusedSubScriberNode(rt_uint32_t *subscriberHdl)
{
    for (rt_uint32_t idx = 0; idx < g_ctx.maxSubscriberCnt; idx++) {
        if (g_ctx.subscribeList[idx].subscriberCb == RT_NULL) {
            *subscriberHdl = (rt_uint32_t)idx;
            return &g_ctx.subscribeList[idx];
        }
    }

    return RT_NULL;
}

static rt_int32_t EVENTHUB_InitEventList()
{
    for (rt_uint32_t eventIdx = 0; eventIdx < HI_EVENTHUB_MAX_EVENT_CNT; eventIdx++) {
        g_ctx.eventList[eventIdx].eventId = APPCOMM_U32_INVALID_VAL;
        memset(g_ctx.eventList[eventIdx].subscriberIdx,
                       APPCOMM_U8_INVALID_VAL, EVENTHUB_MAX_FUNC_PER_EVENT);
    }

    g_ctx.maxEventCnt = (rt_uint8_t)HI_EVENTHUB_MAX_EVENT_CNT;

    return RT_EOK;
}

static void EVENTHUB_DeinitEventList(void)
{
    for (rt_uint32_t eventIdx = 0; eventIdx < HI_EVENTHUB_MAX_EVENT_CNT; eventIdx++) {
        g_ctx.eventList[eventIdx].eventId = APPCOMM_U32_INVALID_VAL;
        memset(g_ctx.eventList[eventIdx].subscriberIdx,
                       APPCOMM_U8_INVALID_VAL, EVENTHUB_MAX_FUNC_PER_EVENT);
    }

    return;
}

static rt_int32_t EVENTHUB_InitSubscriberList()
{
    g_ctx.maxSubscriberCnt = (rt_uint8_t)HI_EVENTHUB_MAX_SUBSCRIBER_CNT;

    return RT_EOK;
}

static void EVENTHUB_DeinitSubscriberList(void)
{
    return;
}

static void EVENTHUB_DoSubscriberCb(eventhub_event_t *event, rt_uint32_t subcriberIdx)
{
    rt_int32_t ret;
    eventhub_subcriber_callback_t subscriberCb;
    void *argv = RT_NULL;

    subscriberCb = g_ctx.subscribeList[subcriberIdx].subscriberCb;
    argv = g_ctx.subscribeList[subcriberIdx].argv;
    if (subscriberCb != RT_NULL) {
        ret = subscriberCb(event, argv);
        if (ret != RT_EOK) {
            LOG_E("EVENTHUB_DoSubscriberCb failed,subscriberCb id :%d.", subcriberIdx);
        }
    }

    return;
}

static rt_int32_t EVENTHUB_ResponseEvent(eventhub_event_t *event)
{
    EVENTHUB_EventNode *node = EVENTHUB_FindUsedEventNode(event->eventId);
    if (node == RT_NULL) {
        LOG_E("err:can not find event:%d.", event->eventId);
        return RT_ERROR;
    }

    rt_uint32_t subcriberIdx;
    for (rt_uint32_t idx = 0; idx < EVENTHUB_MAX_FUNC_PER_EVENT; idx++) {
        subcriberIdx = node->subscriberIdx[idx];
        if (subcriberIdx != EVENTHUB_INVALID_SUBSCRIBER_ID) {
            EVENTHUB_DoSubscriberCb(event, subcriberIdx);
        }
    }

    return RT_EOK;
}

static rt_int32_t EVENTHUB_Publish(eventhub_event_t *event)
{
    rt_int32_t ret;

    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

#ifdef EVENTHUB_SUPPORT_QUEUE
    if (g_ctx.eventQueue == APPCOMM_U32_INVALID_VAL) {
        return RT_ERROR;
    }

    if (msg_queue_is_full(g_ctx.eventQueue) == RT_FALSE) {
        ret = (int)msg_queue_send(g_ctx.eventQueue, event, 50, sizeof(eventhub_event_t)); /* 50:timeout */
        if (ret != RT_EOK) {
            LOG_E("msg_queue_send failed[0x%x],eventId:0x%x.", ret, event->eventId);
            return RT_ERROR;
        }
    } else {
        LOG_E("msg queue is full.");
        return RT_ERROR;
    }
#else
    ret = EVENTHUB_ResponseEvent(event);
#endif

    return ret;
}

#ifdef EVENTHUB_SUPPORT_QUEUE
static void *EVENTHUB_EventProcThread(void *arg)
{
    HI_UNREF_PARAM(arg);
    eventhub_event_t event = { 0 };
    rt_uint32_t msgSize;

    while ((g_ctx.isStarted == 1) || (msg_queue_is_full(g_ctx.eventQueue) == RT_TRUE)) {
        msgSize = (rt_uint32_t)sizeof(eventhub_event_t);
        msg_queue_wait(g_ctx.eventQueue, &event, HI_SYS_WAIT_FOREVER, &msgSize);
        if (msgSize == (rt_uint32_t)sizeof(eventhub_event_t)) {
            EVENTHUB_ResponseEvent(&event);
        } else {
            LOG_E("received unknown event,size:%u.", msgSize);
        }
    }

    LOG_D("EVENTHUB_EventProcThread exit.");

    return RT_NULL;
}

static rt_int32_t EVENTHUB_CreateEventQueue(void)
{
    if (g_ctx.eventQueue != APPCOMM_U32_INVALID_VAL) {
        LOG_E("err:g_ctx.eventQueue is not null.");
        return RT_ERROR;
    }

    rt_int32_t ret = (int)msg_queue_create(&g_ctx.eventQueue, EVENTHUB_MAX_QUEUE_SIZE, sizeof(eventhub_event_t));
    if (ret != RT_EOK) {
        g_ctx.eventQueue = APPCOMM_U32_INVALID_VAL;
        LOG_E("hi_msg_queue_create failed.");
        return RT_ERROR;
    }

    g_ctx.isStarted = 1;

    hi_task_attr attr = { 0 };
    attr.task_prio = EVENTHUB_TASK_PRIORITY;
    attr.task_name = "eventhub_thread";
    attr.stack_size = EVENTHUB_TASK_STACK_SIZE;
    task_create(&g_ctx.eventProcThread, &attr, EVENTHUB_EventProcThread, RT_NULL);

    return RT_EOK;
}

static void EVENTHUB_DestroyEventQueue(void)
{
    g_ctx.isStarted = 0;

    HI_TaskJoin(g_ctx.eventProcThread);
    g_ctx.eventProcThread = APPCOMM_U32_INVALID_VAL;

    msg_queue_delete(g_ctx.eventQueue);
    g_ctx.eventQueue = APPCOMM_U32_INVALID_VAL;

    return;
}
#endif

static rt_int32_t EVENTHUB_InitContex(void)
{
    g_ctx.isStarted = 1;

    rt_int32_t ret = EVENTHUB_InitSubscriberList();
    if (ret != RT_EOK) {
        LOG_E("err:EVENTHUB_InitSubscriberList failed.");
        g_ctx.isStarted = 0;
        return RT_ERROR;
    }

    ret = EVENTHUB_InitEventList();
    if (ret != RT_EOK) {
        LOG_E("err:EVENTHUB_InitEventList failed.");
        EVENTHUB_DeinitSubscriberList();
        g_ctx.isStarted = 0;
        return RT_ERROR;
    }

#ifdef EVENTHUB_SUPPORT_QUEUE
    g_ctx.eventQueue = APPCOMM_U32_INVALID_VAL;
    ret = EVENTHUB_CreateEventQueue();
#endif

    return ret;
}

static rt_int32_t EVENTHUB_DeinitContex(void)
{
#ifdef EVENTHUB_SUPPORT_QUEUE
    EVENTHUB_DestroyEventQueue();
#endif

    EVENTHUB_DeinitEventList();
    EVENTHUB_DeinitSubscriberList();

    return RT_EOK;
}

static rt_int32_t EVENTHUB_Init(void)
{
    rt_int32_t ret;

    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted != RT_TRUE, RT_ERROR, "module has started");

    ret = EVENTHUB_InitContex();
    APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, ret, "EVENTHUB_InitContex");

    ret = EVENTHUB_InitTrigger();
    APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, ret, "EVENTHUB_InitTrigger");

    return ret;
}

static rt_int32_t EVENTHUB_Deinit(void)
{
    rt_int32_t ret;

    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    g_ctx.isStarted = 0;

    ret = EVENTHUB_DeinitContex();
    APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, ret, "EVENTHUB_DeinitContex");

    EVENTHUB_DeinitTrigger();
    APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, ret, "EVENTHUB_DeinitTrigger");

    return ret;
}

static rt_int32_t EVENTHUB_Register(event_id_t eventId)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    EVENTHUB_EventNode *node = EVENTHUB_FindUsedEventNode(eventId);
    if (node != RT_NULL) {
        LOG_E("eventId:%d has been registed already.", eventId);
        return RT_EOK;
    }

    node = EVENTHUB_FindUnusedEventNode();
    if (node == RT_NULL) {
        LOG_E("err:max event cnt is %d.", g_ctx.maxEventCnt);
        return RT_ERROR;
    }

    if (node->eventId == APPCOMM_U32_INVALID_VAL) {
        node->eventId = eventId;
    } else {
        LOG_E("err:node->eventId(%u) is not invalid.", node->eventId);
        return RT_ERROR;
    }

    return RT_EOK;
}

static rt_int32_t EVENTHUB_UnRegister(event_id_t eventId)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    EVENTHUB_EventNode *node = EVENTHUB_FindUsedEventNode(eventId);
    if (node == RT_NULL) {
        return RT_EOK;
    }

    memset(node, 0x0, sizeof(EVENTHUB_EventNode));
    node->eventId = APPCOMM_U32_INVALID_VAL;

    return RT_EOK;
}

static rt_int32_t EVENTHUB_CreateSubscriber(eventhub_subscriber_t *subscriber, rt_uint32_t *subscriberHdl)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    eventhub_subcriber_callback_t addr = subscriber->subscriberCb;
    EVENTHUB_SubscriberNode *node = EVENTHUB_FindUsedSubScriberNodeByAddr(addr);
    if (node != RT_NULL) {
        LOG_E("subscriberCb[%p] has been created already.", addr);
        return RT_EOK;
    }

    node = EVENTHUB_FindUnusedSubScriberNode(subscriberHdl);
    if (node == RT_NULL) {
        LOG_E("err:max subscribers are %d.", g_ctx.maxSubscriberCnt);
        return RT_ERROR;
    }

    node->subscriberCb = subscriber->subscriberCb;
    node->argv = subscriber->argv;

    return RT_EOK;
}

static rt_int32_t EVENTHUB_DestroySubscriber(rt_uint32_t subscriberHdl)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    if (subscriberHdl >= g_ctx.maxSubscriberCnt) {
        LOG_E("err:invalid subscriberHdl:%u.", subscriberHdl);
        return RT_ERROR;
    }

    rt_uint8_t subhdl = (rt_uint8_t)subscriberHdl;
    EVENTHUB_SubscriberNode *node = EVENTHUB_FindUsedSubScriberNodeByHandle(subhdl);
    if (node == RT_NULL) {
        LOG_E("err:can not find subscriber.");
    } else {
      memset(node, 0x0, sizeof(EVENTHUB_SubscriberNode));
    }

    for (rt_uint32_t eventIdx = 0; eventIdx < g_ctx.maxEventCnt; eventIdx++) {
        for (rt_uint32_t subIdx = 0; subIdx < EVENTHUB_MAX_FUNC_PER_EVENT; subIdx++) {
            if (g_ctx.eventList[eventIdx].subscriberIdx[subIdx] == subhdl) {
                g_ctx.eventList[eventIdx].subscriberIdx[subIdx] = EVENTHUB_INVALID_SUBSCRIBER_ID;
            }
        }
    }

    return RT_EOK;
}

static rt_int32_t EVENTHUB_Subscribe(rt_uint32_t subscriberHdl, event_id_t eventId)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    EVENTHUB_EventNode *node = EVENTHUB_FindUsedEventNode(eventId);
    if (node == RT_NULL) {
        LOG_E("event id:%d has not been registered.", eventId);
        return RT_ERROR;
    }

    rt_uint32_t subIdx;
    for (subIdx = 0; subIdx < EVENTHUB_MAX_FUNC_PER_EVENT; subIdx++) {
        if (node->subscriberIdx[subIdx] == (rt_uint8_t)subscriberHdl) {
            break;
        }
    }

    if (subIdx != EVENTHUB_MAX_FUNC_PER_EVENT) {
        return RT_EOK;
    }

    for (subIdx = 0; subIdx < EVENTHUB_MAX_FUNC_PER_EVENT; subIdx++) {
        if (node->subscriberIdx[subIdx] == EVENTHUB_INVALID_SUBSCRIBER_ID) {
            break;
        }
    }

    if (subIdx == EVENTHUB_MAX_FUNC_PER_EVENT) {
        LOG_E("err:one event id can only has %d subscribers.", EVENTHUB_MAX_FUNC_PER_EVENT);
        return RT_ERROR;
    } else {
        node->subscriberIdx[subIdx] = (rt_uint8_t)subscriberHdl;
        return RT_EOK;
    }

    return RT_EOK;
}

static rt_int32_t EVENTHUB_UnSubscribe(rt_uint32_t subscriberHdl, event_id_t eventId)
{
    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    EVENTHUB_EventNode *node = EVENTHUB_FindUsedEventNode(eventId);
    if (node == RT_NULL) {
        LOG_E("event id:%d has not been registered.", eventId);
        return RT_ERROR;
    }

    rt_uint32_t subIdx;
    for (subIdx = 0; subIdx < EVENTHUB_MAX_FUNC_PER_EVENT; subIdx++) {
        if (node->subscriberIdx[subIdx] == (rt_uint8_t)subscriberHdl) {
            node->subscriberIdx[subIdx] = EVENTHUB_INVALID_SUBSCRIBER_ID;
        }
    }

    return RT_EOK;
}

int eventhub_register(event_id_t eventId)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_Register(eventId);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_init(void)
{
    rt_int32_t ret;

    MUTEX_INIT_LOCK(GetEventHubCtx()->mutex);

    ret = EVENTHUB_Init();

    return ret;
}

int eventhub_deinit(void)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_Deinit();
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    MUTEX_DESTROY(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_unregister(event_id_t eventId)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_UnRegister(eventId);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_publish(eventhub_event_t *event)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_Publish(event);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_create_subscriber(eventhub_subscriber_t *subscriber, rt_uint32_t *subscriberHdl)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_CreateSubscriber(subscriber, subscriberHdl);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_destroy_subscriber(rt_uint32_t subscriberHdl)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_DestroySubscriber(subscriberHdl);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_subscribe(rt_uint32_t subscriberHdl, event_id_t eventId)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_Subscribe(subscriberHdl, eventId);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_unsubscribe(rt_uint32_t subscriberHdl, event_id_t eventId)
{
    rt_int32_t ret;

    MUTEX_LOCK(GetEventHubCtx()->mutex);
    ret = EVENTHUB_UnSubscribe(subscriberHdl, eventId);
    MUTEX_UNLOCK(GetEventHubCtx()->mutex);

    return ret;
}

int eventhub_dump_event_and_subscriberlist(void)
{
#ifdef EVENTHUB_DEBUG
    rt_uint32_t idx;

    APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(GetEventHubCtx()->isStarted == RT_TRUE, RT_ERROR, "module not started");

    LOG_D("---------------g_ctx.eventList---------------\n");
    for (idx = 0; idx < g_ctx.maxEventCnt; idx++) {
        printf("[%d]: [", g_ctx.eventList[idx].eventId);
        for (rt_uint32_t subIdx = 0; subIdx < EVENTHUB_MAX_FUNC_PER_EVENT; subIdx++) {
            LOG_D("%5d ", g_ctx.eventList[idx].subscriberIdx[subIdx]);
        }
        LOG_D("]\n");
    }

    LOG_D("---------------g_ctx.subscribeList---------------\n");
    for (idx = 0; idx < g_ctx.maxSubscriberCnt; idx++) {
        LOG_D("[%p] ", g_ctx.subscribeList[idx].subscriberCb);
    }
    LOG_D("\n");
#endif

    return RT_EOK;
}
