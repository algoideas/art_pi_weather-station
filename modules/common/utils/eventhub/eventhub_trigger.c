/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#include "eventhub_trigger.h"
#include "eventhub.h"
#include "appcomm.h"

#include <rtthread.h>


#define DBG_TAG     "EVENTHUB"
#define DBG_LVL     DBG_ERROR
#include <rtdbg.h>

#define EVENTHUB_MAX_TRIGGER_NUM    23
#define EVENTHUB_DEINIT_TRIGGER     23
#define EVENTHUB_WAIT_MASK          0xFFFFFF
#define EVENTHUB_TRIGGER_STACK_SIZE 0xc00
#define EVENTHUB_TRIGGER_PRIORITY   20

typedef struct {
    rt_bool_t isInited;
    rt_bool_t isWaitTaskRun;
    struct rt_event eventHdl;
    rt_thread_t waitTaskId;
    rt_uint32_t triggerBits;
    eventhub_event_trigger_cfg_t triggerCbList[EVENTHUB_MAX_TRIGGER_NUM];
} EVENTHUB_Trigger_Ctx;

static EVENTHUB_Trigger_Ctx g_eventHubTriggerCtx = { 0 };

static inline EVENTHUB_Trigger_Ctx *GetEventHubTriggerCtx(void)
{
    return &g_eventHubTriggerCtx;
}

static void EVENTHUB_TRIGGER_InitTriggerCtx(EVENTHUB_Trigger_Ctx *triggerCtx)
{
    triggerCtx->isInited = RT_FALSE;
    triggerCtx->isWaitTaskRun = RT_FALSE;
    triggerCtx->triggerBits = 0;

    for (rt_uint32_t index = 0; index < EVENTHUB_MAX_TRIGGER_NUM; index++) {
        triggerCtx->triggerCbList[index].eventTriggerCb = RT_NULL;
        triggerCtx->triggerCbList[index].priv = RT_NULL;
    }

    return;
}

static void EVENTHUB_TRIGGER_TriggerEvent(rt_uint8_t triggerHdl)
{
    rt_uint32_t mask = 1 << triggerHdl;

    if (&GetEventHubTriggerCtx()->eventHdl != RT_NULL) {
        rt_int32_t ret = rt_event_send(&GetEventHubTriggerCtx()->eventHdl, mask);
        APPCOMM_LOG_IF_EXPR_FALSE(ret == RT_EOK, "rt_event_send");
    } else {
        LOG_W("eventHdl should been invalid.");
    }

    return;
}

static rt_uint32_t EVENTHUB_TRIGGER_WaitEventBits(rt_uint32_t eventBit)
{
    rt_uint32_t event;
    rt_int32_t ret = rt_event_recv(&GetEventHubTriggerCtx()->eventHdl, eventBit,
                                       RT_EVENT_FLAG_AND | RT_EVENT_FLAG_CLEAR,
                                       RT_WAITING_FOREVER, 
                                       &event);
    if (ret != RT_EOK) {
        LOG_E("rt_event_recv failed(0x%x).", ret);
        return 0;
    }

    return event;
}

static void EVENTHUB_TRIGGER_ResponseEvent(rt_uint32_t event)
{
    rt_uint32_t index;
    rt_uint32_t mask;

    for (index = 0; index < EVENTHUB_MAX_TRIGGER_NUM; index++) {
        mask = 1 << index;
        if ((event & mask) != 0) {
            eventhub_event_trigger_callback_t triggerCb = GetEventHubTriggerCtx()->triggerCbList[index].eventTriggerCb;
            if (triggerCb != RT_NULL) {
                triggerCb(GetEventHubTriggerCtx()->triggerCbList[index].priv);
            }
        }
    }

    return;
}

static void EVENTHUB_TRIGGER_ProcTriggerEventThread(void *data)
{
    LOG_D("EVENTHUB_TRIGGER_ProcTriggerEventThread enter\n");

    rt_uint32_t event;

    while (GetEventHubTriggerCtx()->isWaitTaskRun == RT_TRUE) {
        event = EVENTHUB_TRIGGER_WaitEventBits(EVENTHUB_WAIT_MASK);
        EVENTHUB_TRIGGER_ResponseEvent(event);
    }

    LOG_D("EVENTHUB_TRIGGER_ProcTriggerEventThread exit ...");

    return ;
}

static rt_int32_t EVENTHUB_TRIGGER_InitCheckThread(void)
{
    LOG_D("EVENTHUB_TRIGGER_InitCheckThread ...");

    rt_int32_t ret = rt_event_init(&GetEventHubTriggerCtx()->eventHdl, "eventhub_event", RT_IPC_FLAG_FIFO);
    APPCOMM_LOG_IF_EXPR_FALSE(ret == RT_EOK, "rt_event_init");

    GetEventHubTriggerCtx()->isWaitTaskRun = RT_TRUE;
    GetEventHubTriggerCtx()->waitTaskId = rt_thread_create("eventhub_trigger", EVENTHUB_TRIGGER_ProcTriggerEventThread,
            RT_NULL, 
            1024,
            14,
            10);
    if (GetEventHubTriggerCtx()->waitTaskId != RT_NULL)
    {
        rt_thread_startup(GetEventHubTriggerCtx()->waitTaskId);
        ret = RT_EOK;
    }
    else
    {
        ret = RT_ERROR;
    }

    APPCOMM_LOG_IF_EXPR_FALSE(ret == RT_EOK, "rt_thread_create");

    return ret;
}

static void EVENTHUB_TRIGGER_DeinitCheckThread(void)
{
    rt_int32_t ret;

    EVENTHUB_Trigger_Ctx *ctx = GetEventHubTriggerCtx();

    APPCOMM_LOG_IF_EXPR_FALSE(ctx->waitTaskId != RT_NULL,
                                 "ctx->checkTskId is not invalid.");

    /* destroy thread */
    ctx->isWaitTaskRun = RT_FALSE;
    EVENTHUB_TRIGGER_TriggerEvent(EVENTHUB_DEINIT_TRIGGER);
    rt_thread_delete(ctx->waitTaskId);
    ctx->waitTaskId = RT_NULL;

    /* delete event */
    ret = rt_event_delete(&ctx->eventHdl);
    APPCOMM_LOG_IF_EXPR_FALSE(ret == RT_EOK, "rt_event_delete");

    LOG_D("EVENTHUB_TRIGGER_DeinitCheckThread ...");

    return;
}

static rt_int32_t EVENTHUB_TRIGGER_FindUnusedHandle(rt_uint8_t *triggerHdl)
{
    for (rt_uint32_t index = 0; index < EVENTHUB_MAX_TRIGGER_NUM; index++) {
        rt_uint32_t mask = 1 << index;
        if ((GetEventHubTriggerCtx()->triggerBits & mask) == 0x0) {
            *triggerHdl = index;
            GetEventHubTriggerCtx()->triggerBits = GetEventHubTriggerCtx()->triggerBits | mask;
            return RT_EOK;
        }
    }

    return RT_ERROR;
}

rt_int32_t EVENTHUB_InitTrigger(void)
{
    EVENTHUB_Trigger_Ctx *ctx = GetEventHubTriggerCtx();
    if (ctx->isInited == RT_TRUE) {
        return RT_EOK;
    }

    EVENTHUB_TRIGGER_InitTriggerCtx(ctx);

    rt_int32_t ret = EVENTHUB_TRIGGER_InitCheckThread();

    ctx->isInited = RT_TRUE;

    return ret;
}

void EVENTHUB_DeinitTrigger(void)
{
    if (GetEventHubTriggerCtx()->isInited == RT_FALSE) {
        return;
    }
    EVENTHUB_TRIGGER_DeinitCheckThread();

    GetEventHubTriggerCtx()->isInited = RT_FALSE;

    return;
}

rt_int32_t eventhub_create_event_trigger(rt_uint8_t *triggerHdl, eventhub_event_trigger_cfg_t *eventTriggerCfg)
{
    rt_int32_t ret = EVENTHUB_TRIGGER_FindUnusedHandle(triggerHdl);
    APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, ret, "exceed max trigger.");

    if (*triggerHdl < EVENTHUB_MAX_TRIGGER_NUM) {
        GetEventHubTriggerCtx()->triggerCbList[*triggerHdl].eventTriggerCb = eventTriggerCfg->eventTriggerCb;
        GetEventHubTriggerCtx()->triggerCbList[*triggerHdl].priv = eventTriggerCfg->priv;
        return RT_EOK;
    }

    return RT_ERROR;
}

rt_int32_t eventhub_trigger_event(rt_uint8_t triggerHdl)
{
    if (triggerHdl >= EVENTHUB_MAX_TRIGGER_NUM) {
        LOG_E("invalid triggerHdl:%d,valid triggerHdl[0-%d].", triggerHdl, EVENTHUB_MAX_TRIGGER_NUM);
        return RT_ERROR;
    }

    EVENTHUB_TRIGGER_TriggerEvent(triggerHdl);

    return RT_EOK;
}

rt_int32_t eventhub_destroy_event_trigger(rt_uint8_t triggerHdl)
{
    if (triggerHdl >= EVENTHUB_MAX_TRIGGER_NUM) {
        LOG_E("invalid triggerHdl:%d,valid triggerHdl[0-%d].", triggerHdl, EVENTHUB_MAX_TRIGGER_NUM);
        return RT_EOK;
    }

    GetEventHubTriggerCtx()->triggerBits &= ~(1 << triggerHdl);
    GetEventHubTriggerCtx()->triggerCbList[triggerHdl].eventTriggerCb = RT_NULL;
    GetEventHubTriggerCtx()->triggerCbList[triggerHdl].priv = RT_NULL;

    return RT_EOK;
}
