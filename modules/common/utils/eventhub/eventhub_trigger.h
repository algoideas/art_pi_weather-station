/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#ifndef MODULES_UTILS_EVENTHUB_EVENTHUB_TRIGGER_H
#define MODULES_UTILS_EVENTHUB_EVENTHUB_TRIGGER_H

#include "eventhub.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

rt_int32_t EVENTHUB_InitTrigger(void);

void EVENTHUB_DeinitTrigger(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* #ifdef __cplusplus */

#endif /* #ifdef __EVENTHUB_TRIGGER_H */
