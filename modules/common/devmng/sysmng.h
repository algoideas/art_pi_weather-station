#ifndef __SYSMNG_H
#define __SYSMNG_H

int sysmng_init(void);

rt_uint32_t get_sys_running_time(void);

#endif  /*__SYSMNG_H */
 
