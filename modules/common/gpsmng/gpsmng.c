/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "lwgps2rtt.h"

#define DBG_TAG "gps"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#define GPS_UART_NAME       "uart1"

//#define GPS_LWGPS_ENABLE
#ifdef GPS_LWGPS_ENABLE
static void gps_thread_entry(void *parameter)
{
    lwgps_t gps_info;

    while (1)
    {
        lwgps2rtt_get_gps_info(&gps_info);   /* Process byte-by-byte */

        if (gps_info.is_valid)
        {
            rt_kprintf("Latitude: %f degrees\r\n", gps_info.latitude);
            rt_kprintf("Longitude: %f degrees\r\n", gps_info.longitude);
            rt_kprintf("Altitude: %f meters\r\n", gps_info.altitude);
        }

        rt_thread_delay(RT_TICK_PER_SECOND / 10);
    }
}
#endif

int gpsmng_init(void)
{
    rt_err_t ret = RT_EOK;

#ifdef GPS_LWGPS_ENABLE
    /* Init GPS */
    lwgps2rtt_init(GPS_UART_NAME);

    rt_thread_t thread = rt_thread_create("gpsmng", gps_thread_entry, RT_NULL, 1024, 25, 20);
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
    else
    {
        ret = RT_ERROR;
    }
#endif

    return ret;
}
