/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo         the first version
 */

#ifndef MODULES_UTILS_APPCOMM_H_
#define MODULES_UTILS_APPCOMM_H_

#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <rtthread.h>
#include <rtdef.h>
#include <rtdbg.h>

#include "pthread.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/* Invalid FD */
#define APPCOMM_FD_INVALID_VAL (-1)

/* Invalid U32 Value */
#define APPCOMM_U32_INVALID_VAL (0xffffffff)

/* Invalid U8 Value */
#define APPCOMM_U8_INVALID_VAL (0xff)

/* General String Length */
#define APPCOMM_COMM_STR_LEN 64

/* Path Maximum Length */
#define APPCOMM_MAX_PATH_LEN 64

/* FileName Maximum Length */
#define APPCOMM_MAX_FILENAME_LEN 64

/** Directory mode */
#define APPCOMM_DIR_MODE (0760)

/* Appfs Flash Path */
#define HI_APPFS_FLASH_PATH  "/flash"

/* Sharefs Sdcard Path */
#define HI_APPFS_SDCARD_PATH "/sdcard"

/* Pointer Check */
#define APPCOMM_RETURN_IF_PTR_NULL(p, errcode)    \
    do {                                             \
        if ((p) == RT_NULL) {                        \
            LOG_E("pointer[%s] is RT_NULL\n", #p);   \
            return (errcode);                        \
        }                                            \
    } while (0)

/* Expression Check */
#define APPCOMM_RETURN_IF_EXPR_FALSE(expr, errcode) \
    do {                                               \
        if ((expr) == RT_FALSE) {                      \
            LOG_E("expr[%s] false\n", #expr);          \
            return (errcode);                          \
        }                                              \
    } while (0)

/* Expression Check With ErrInformation */
#define APPCOMM_LOG_AND_RETURN_IF_EXPR_FALSE(expr, errcode, errstring) \
    do {                                                                  \
        if ((expr) == RT_FALSE) {                                         \
            LOG_E("[%s] failed\n", (errstring));                          \
            return (errcode);                                             \
        }                                                                 \
    } while (0)

/* Return Result Check */
#define APPCOMM_RETURN_IF_FAIL(ret, errcode)       \
    do {                                              \
        if ((ret) != RT_EOK) {                    \
            LOG_E("Error Code: [0x%08X]\n\n", (ret)); \
            return (errcode);                         \
        }                                             \
    } while (0)

/* Return Result Check With ErrInformation */
#define APPCOMM_LOG_AND_RETURN_IF_FAIL(ret, errcode, errstring) \
    do {                                                           \
        if ((ret) != RT_EOK) {                                 \
            LOG_E("[%s] failed[0x%08X]\n", (errstring), (ret));    \
            return (errcode);                                      \
        }                                                          \
    } while (0)


/* Expression Check Without Return */
#define APPCOMM_LOG_IF_EXPR_FALSE(expr, errstring) \
    do {                                              \
        if ((expr) == RT_FALSE) {                     \
            LOG_E("[%s] failed\n", (errstring));      \
        }                                             \
    } while (0)

/* Range Check */
#define APPCOMM_CHECK_RANGE(value, min, max) (((value) <= (max) && (value) >= (min)) ? 1 : 0)

/* Memory Safe Free */
#define APPCOMM_SAFE_FREE(p) \
    do {                        \
        if ((p) != RT_NULL) {   \
            free(p);            \
            (p) = RT_NULL;      \
        }                       \
    } while (0)

/* Value Align */
#define APPCOMM_ALIGN(value, base) (((value) + (base)-1) / (base) * (base))

/* strcmp enum string and value */
#define APPCOMM_STRCMP_ENUM(enumStr, enumValue) strncmp(enumStr, #enumValue, APPCOMM_COMM_STR_LEN)

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#endif

#ifndef MAX
#define MAX(a, b) (((a) < (b)) ? (b) : (a))
#endif

#ifndef MIN
#define MIN(a, b) (((a) > (b)) ? (b) : (a))
#endif

#ifndef SWAP
#define SWAP(a, b) ((a) = (a) + (b), (b) = (a) - (b), (a) = (a) - (b))
#endif

/* Mutex Lock */
#define MUTEX_INIT_LOCK(mutex)                       \
    do {                                                \
        (void)pthread_mutex_init(&(mutex), RT_NULL); \
    } while (0)
#define MUTEX_LOCK(mutex)                   \
    do {                                       \
        (void)pthread_mutex_lock(&(mutex)); \
    } while (0)
#define MUTEX_UNLOCK(mutex)                   \
    do {                                         \
        (void)pthread_mutex_unlock(&(mutex)); \
    } while (0)
#define MUTEX_DESTROY(mutex)                   \
    do {                                          \
        (void)pthread_mutex_destroy(&(mutex)); \
    } while (0)

/* Cond */
#define COND_INIT(cond)                                              \
    do {                                                                \
        pthread_condattr_t condattr;                                    \
        (void)pthread_condattr_init(&condattr);                      \
        (void)pthread_condattr_setclock(&condattr, CLOCK_MONOTONIC); \
        (void)pthread_cond_init(&(cond), &condattr);                 \
        (void)pthread_condattr_destroy(&condattr);                   \
    } while (0)
#define COND_TIMEDWAIT(cond, mutex, usec)                     \
    do {                                                         \
        struct timespec ts;                                      \
        memset(&ts, 0, sizeof(struct timespec));                 \
        (void)clock_gettime(CLOCK_MONOTONIC, &ts);            \
        ts.tv_sec += ((usec) / 1000000LL);                       \
        ts.tv_nsec += ((usec) * 1000LL % 1000000000LL);          \
        (void)pthread_cond_timedwait(&(cond), &(mutex), &ts); \
    } while (0)
#define COND_TIMEDWAIT_WITH_RETURN(cond, mutex, usec, ret) \
    do {                                                      \
        struct timespec ts;                                   \
        memset(&ts, 0, sizeof(struct timespec));              \
        (void)clock_gettime(CLOCK_MONOTONIC, &ts);         \
        ts.tv_sec += ((usec) / 1000000LL);                    \
        ts.tv_nsec += ((usec) * 1000LL % 1000000000LL);       \
        ret = pthread_cond_timedwait(&(cond), &(mutex), &ts); \
    } while (0)

#define COND_WAIT(cond, mutex)                      \
    do {                                               \
        (void)pthread_cond_wait(&(cond), &(mutex)); \
    } while (0)
#define COND_SIGNAL(cond)                   \
    do {                                       \
        (void)pthread_cond_signal(&(cond)); \
    } while (0)
#define COND_DESTROY(cond)                   \
    do {                                        \
        (void)pthread_cond_destroy(&(cond)); \
    } while (0)

/* App Event BaseId : [28bit~31bit] unique */
#define APPCOMM_EVENT_BASEID 0x10000000L

/* App Event ID Rule [ --base[4bit]--|--module[8bit]--|--event[20bit]--]
 * module : module enum value [HI_APP_Module]
 * event_code : event code in specified module, unique in module
 */
#define APPCOMM_EVENT_ID(module, event) \
    ((rt_int32_t)((APPCOMM_EVENT_BASEID) | ((module) << 20) | (event)))

/* App Error BaseId : [28bit~31bit] unique */
#define APPCOMM_ERR_BASEID 0x80000000L

/* App Error Code Rule [ --base[4bit]--|--module[8bit]--|--error[20bit]--]
 * module : module enum value [HI_APP_Module]
 * event_code : event code in specified module, unique in module
 */
#define APPCOMM_ERR_ID(module, err) \
    ((rt_int32_t)((APPCOMM_ERR_BASEID) | ((module) << 20) | (err)))

/* App MSG BaseId : [28bit~31bit] unique */
#define APPCOMM_MSG_BASEID 0x20000000L

/* App MSG ID Rule [ --base[4bit]--|--module[8bit]--|--msgid[20bit]--]
 * module : module enum value [HI_APP_Module]
 * msgid : msg code in specified module, unique in module
 */
#define APPCOMM_MSG_ID(module, msgid) \
    ((rt_int32_t)((APPCOMM_MSG_BASEID) | ((module) << 20) | (msgid)))


/* App Module ID */
typedef enum {
    APP_MOD_KEY = 0,
    APP_MOD_LED,
    APP_MOD_DHT,
    APP_MOD_WIFI,
    APP_MOD_SVR,
    APP_MOD_BUTT
}APP_Module;


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* End of #ifndef __APPCOMM_H */

