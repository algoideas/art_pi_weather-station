/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author          Notes
 * 2020-10-31     Algo            the first version.
 * Note:
 * Now only support DHT11 temp sensor
 */

#ifndef __TEMP_SENSOR_H
#define __TEMP_SENSOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
#include <drv_common.h>
#include <rtdevice.h>
#include <board.h>

/* define dht11 wiring pin: PI5 */
#define DHT11_DATA_PIN      GET_PIN(I, 5)

typedef struct temp_sensor_info {
    int32_t temp;
    int32_t humi;
}temp_sensor_info_t;

int temp_sensor_init(void);
int temp_sensor_reset(void);
temp_sensor_info_t *temp_sensor_info(void);

#ifdef __cplusplus
}
#endif
#endif  /*__TEMP_SENSOR_H */
