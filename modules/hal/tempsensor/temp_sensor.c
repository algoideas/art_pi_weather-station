/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-19     Algo         the first version
 */

#include <rtdef.h>
#include "sensor.h"
#include "sensor_dallas_dht11.h"
#include "temp_sensor.h"

#define DBG_TAG "sensor.dht11"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

static temp_sensor_info_t sensor_info;
static rt_device_t temp_dev = RT_NULL;

static int dht_temp_read(void)
{
    struct rt_sensor_data sensor_data;
    rt_size_t ret;

    ret = rt_device_read(temp_dev, 0, &sensor_data, 1);
    if (ret != 1)
    {
        LOG_E("read data failed! result is %d\n", ret);
        rt_device_close(temp_dev);
    }
    else
    {
        sensor_info.temp = (sensor_data.data.temp & 0xffff) >> 0;      // get temp
        sensor_info.humi = (sensor_data.data.temp & 0xffff0000) >> 16; // get humi
        LOG_D("temp [%d], humi [%d]", sensor_info.temp, sensor_info.humi);
    }

    return 0;
}

static void dht_temp_entry(void *parameter)
{
    rt_uint8_t get_data_freq = 1; /* 1Hz */

    temp_dev = rt_device_find("temp_dht11");
    if (temp_dev == RT_NULL)
    {
        LOG_E("can not find temp_dht11!\n");
        return;
    }

    if (rt_device_open(temp_dev, RT_DEVICE_FLAG_RDWR) != RT_EOK)
    {
        LOG_E("open device failed!\n");
        return;
    }

    rt_device_control(temp_dev, RT_SENSOR_CTRL_SET_ODR, (void *) (&get_data_freq));

    while (1)
    {
        dht_temp_read();
        rt_thread_delay(10 * 1000);
    }
}

static int dht_startup(void)
{
    rt_thread_t dht11_thread;

    dht11_thread = rt_thread_create("dht_temp", dht_temp_entry,
            RT_NULL,
            1024,
            RT_THREAD_PRIORITY_MAX / 2,
            20);
    if (dht11_thread != RT_NULL)
    {
        rt_thread_startup(dht11_thread);
    }
    else
    {
        LOG_E("rt_thread_create dht_temp failed!");
        return RT_ERROR;
    }

    return RT_EOK;
}

temp_sensor_info_t *temp_sensor_info(void)
{
    return &sensor_info;
}

int temp_sensor_reset(void)
{
    rt_pin_mode(DHT11_DATA_PIN, PIN_MODE_OUTPUT);

    rt_pin_write(DHT11_DATA_PIN, PIN_LOW);
    rt_thread_mdelay(20); /* 20ms */

    rt_pin_write(DHT11_DATA_PIN, PIN_HIGH);
    rt_hw_us_delay(30); /* 30us*/

    return RT_EOK;
}

int temp_sensor_init(void)
{
    struct rt_sensor_config cfg;
    int result;

    cfg.intf.user_data = (void *) DHT11_DATA_PIN;
    result = rt_hw_dht11_init("dht11", &cfg);
    if (RT_EOK != result)
    {
        LOG_E("rt_hw_dht11_init failed! result is %d\n", result);
        return RT_ERROR;
    }

    result = dht_startup();
    if (RT_EOK != result)
    {
        LOG_E("dht_startup failed! result is %d\n", result);
        return RT_ERROR;
    }

    return RT_EOK;
}

MSH_CMD_EXPORT_ALIAS(dht_temp_read, temp, get dth11 temp and humi);
