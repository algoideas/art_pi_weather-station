

#ifndef MODULES_SENSOR_LPS22HH_H__
#define MODULES_SENSOR_LPS22HH_H__

#include "sensor.h"
#include "lps22hh.h"

/*IIC slave address write and read*/
/*SD0 connect to power supply*/
#define LPS22HH_ADDR_W_1			0xBAU
#define LPS22HH_ADDR_R_1			0xBBU

/*SD0 connect to ground*/
#define LPS22HH_ADDR_W_0			0xB8U
#define LPS22HH_ADDR_R_0			0xB9U

#define LPS22HH_ADDR_DEFAULT        (0xB8 >> 1)

#define LPS22HH_CS_PIN              GET_PIN(A, 8)

#define LPS22HH_DEVICE_NAME         "spi20"

int press_sensor_init(void);

#endif
