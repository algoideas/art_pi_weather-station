#include <rtdef.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <drv_common.h>
#include <modules/hal/presssensor/press_sensor.h>

#define DBG_ENABLE
#define DBG_LEVEL DBG_LOG
#define DBG_SECTION_NAME  "sensor.st.lps22hh"
#define DBG_COLOR
#include <rtdbg.h>

static LPS22HH_Object_t lps22hh;
static struct rt_spi_device *spi_dev;
    
static int32_t spi2_dev_init(void)
{
    return 0;
}

static int32_t lps22hh_get_tick(void)
{
    return rt_tick_get();
}

static int rt_spi_write_reg(uint16_t addr, uint16_t reg, uint8_t *data, uint16_t len)
{
    rt_uint8_t tmp = reg;
    rt_size_t size = 0;

    rt_pin_write(LPS22HH_CS_PIN, PIN_LOW);

    size = rt_spi_transfer(spi_dev, &tmp, data, len);
    if (0 == size)
    {
        LOG_E("rt_spi_transfer error. %d\r\n",len); 
        return -RT_ERROR;
    }

    return RT_EOK;
}

static int rt_spi_read_reg(uint16_t addr, uint16_t reg, uint8_t *data, uint16_t len)
{
    rt_uint8_t tmp = reg;
    rt_size_t size = 0;

    rt_pin_write(LPS22HH_CS_PIN, PIN_HIGH);

    size = rt_spi_transfer(spi_dev, &tmp, data, len);
    if (0 == size)
    {
        LOG_E("rt_spi_transfer error. %d\r\n",len); 
        return -RT_ERROR;
    }

    return RT_EOK;
}

static rt_err_t _lps22hh_set_odr(rt_sensor_t sensor, rt_uint16_t odr)
{
    if (sensor->info.type == RT_SENSOR_CLASS_BARO)
    {
        LPS22HH_PRESS_SetOutputDataRate(&lps22hh, odr);
    }
    else if (sensor->info.type == RT_SENSOR_CLASS_TEMP)
    {
        LPS22HH_TEMP_SetOutputDataRate(&lps22hh, odr);
    }

    return RT_EOK;
}

static rt_err_t _lps22hh_set_mode(rt_sensor_t sensor, rt_uint8_t mode)
{
    if (mode == RT_SENSOR_MODE_POLLING)
    {
        LPS22HH_FIFO_Set_Mode(&lps22hh, LPS22HH_FIFO_BYPASS_MODE);
//        LPS22HH_FIFO_Usage(&lps22hh, 0);
    }
    else if (mode == RT_SENSOR_MODE_INT)
    {
        LPS22HH_FIFO_Set_Mode(&lps22hh, LPS22HH_FIFO_BYPASS_MODE);
        LPS22HH_FIFO_Set_Interrupt(&lps22hh, 0);
    }
    else if (mode == RT_SENSOR_MODE_FIFO)
    {
        LPS22HH_FIFO_Set_Mode(&lps22hh, LPS22HH_FIFO_MODE);
        LPS22HH_FIFO_Set_Watermark_Level(&lps22hh, 32);
        LPS22HH_FIFO_Usage(&lps22hh, 1);
        LPS22HH_FIFO_Set_Interrupt(&lps22hh, 2);
    }
    else
    {
        LOG_W("Unsupported mode, code is %d", mode);
        return -RT_ERROR;
    }
    return RT_EOK;
}

static rt_err_t _lps22hh_set_power(rt_sensor_t sensor, rt_uint8_t power)
{
    if (power == RT_SENSOR_POWER_DOWN)
    {
        if (sensor->info.type == RT_SENSOR_CLASS_BARO)
        {
            LPS22HH_PRESS_Disable(&lps22hh);
        }
        else if (sensor->info.type == RT_SENSOR_CLASS_TEMP)
        {
            LPS22HH_TEMP_Disable(&lps22hh);
        }
    }
    else if (power == RT_SENSOR_POWER_NORMAL)
    {
        if (sensor->info.type == RT_SENSOR_CLASS_BARO)
        {
            LPS22HH_PRESS_Enable(&lps22hh);
        }
        else if (sensor->info.type == RT_SENSOR_CLASS_TEMP)
        {
            LPS22HH_TEMP_Enable(&lps22hh);
        }
    }
    else
    {
        LOG_W("Unsupported mode, code is %d", power);
        return -RT_ERROR;
    }
    return RT_EOK;
}

static rt_err_t _lps22hh_clear_int(struct rt_sensor_device *sensor)
{
    if (sensor->config.mode == RT_SENSOR_MODE_FIFO)
    {
        _lps22hh_set_mode(sensor, RT_SENSOR_MODE_POLLING);
        _lps22hh_set_mode(sensor, RT_SENSOR_MODE_FIFO);
    }
    LOG_D("clear int");
    return 0;
}

static rt_size_t _lps22hh_polling_get_data(rt_sensor_t sensor, struct rt_sensor_data *data)
{
    if (sensor->info.type == RT_SENSOR_CLASS_BARO)
    {
        float baro;
        LPS22HH_PRESS_GetPressure(&lps22hh, &baro);

        data->type = RT_SENSOR_CLASS_BARO;
        data->data.baro = baro * 100;
        data->timestamp = rt_sensor_get_ts();
    }
    else if (sensor->info.type == RT_SENSOR_CLASS_TEMP)
    {
        float temp_value;

        LPS22HH_TEMP_GetTemperature(&lps22hh, &temp_value);

        data->type = RT_SENSOR_CLASS_TEMP;
        data->data.temp = temp_value * 10;
        data->timestamp = rt_sensor_get_ts();
    }

    return 1;
}

static rt_size_t _lps22hh_fifo_get_data(rt_sensor_t sensor, struct rt_sensor_data *data, rt_size_t len)
{
    float baro,temp;
    rt_uint8_t i;
    struct rt_sensor_data *other_data;
    rt_sensor_t other_sensor;
    
    if (sensor == sensor->module->sen[0])
        other_sensor = sensor->module->sen[1];
    else
        other_sensor = sensor->module->sen[0];
        
    other_data = other_sensor->data_buf;
    
    if (len > 32) len = 32;
    
    for(i = 0; i < len; i++)
    {
        if (LPS22HH_FIFO_Get_Data(&lps22hh, &baro, &temp) == 0)
        {
            if (sensor->info.type == RT_SENSOR_CLASS_BARO)
            {
                data[i].type = RT_SENSOR_CLASS_BARO;
                data[i].data.baro = baro * 100;
                data[i].timestamp = rt_sensor_get_ts();
                
                other_data[i].type = RT_SENSOR_CLASS_TEMP;
                other_data[i].data.temp = temp * 10;
                other_data[i].timestamp = rt_sensor_get_ts();
            }
            else
            {
                data[i].type = RT_SENSOR_CLASS_TEMP;
                data[i].data.temp = temp * 10;
                data[i].timestamp = rt_sensor_get_ts();
                
                other_data[i].type = RT_SENSOR_CLASS_BARO;
                other_data[i].data.baro = baro * 100;
                other_data[i].timestamp = rt_sensor_get_ts();
            }
        }
        else
            break;
    }
    other_sensor->data_len = i * sizeof(struct rt_sensor_data);
    _lps22hh_clear_int(sensor);
    return i;
}

static rt_err_t _lps22hh_baro_init(struct rt_sensor_intf *intf)
{
    LPS22HH_IO_t io_ctx;
    rt_uint8_t id;

    spi_dev = (struct rt_spi_bus *)rt_device_find(intf->dev_name);
    if (spi_dev == RT_NULL)
    {
        rt_kprintf("rt_device_find failed");
        return -RT_ERROR;
    }

    /* Configure the baroelero driver */
    io_ctx.BusType     = LPS22HH_SPI_4WIRES_BUS; /* SPI2 */
    io_ctx.Address     = 0xff;
    io_ctx.Init        = spi2_dev_init;
    io_ctx.DeInit      = spi2_dev_init;
    io_ctx.ReadReg     = rt_spi_read_reg;
    io_ctx.WriteReg    = rt_spi_write_reg;
    io_ctx.GetTick     = lps22hh_get_tick;

    if (LPS22HH_RegisterBusIO(&lps22hh, &io_ctx) != LPS22HH_OK)
    {
        LOG_E("LPS22HH_RegisterBusIO failed");
        return -RT_ERROR;
    }

    if (LPS22HH_ReadID(&lps22hh, &id) != LPS22HH_OK)
    {
        LOG_E("read id failed");
        return -RT_ERROR;
    }

    LOG_I("ID [0x%x]", id);

    if (LPS22HH_Init(&lps22hh) != LPS22HH_OK)
    {
        LOG_E("lps22hh init failed");
        return -RT_ERROR;
    }

    return RT_EOK;
}
static rt_size_t lps22hh_fetch_data(struct rt_sensor_device *sensor, void *buf, rt_size_t len)
{
    if (sensor->config.mode == RT_SENSOR_MODE_POLLING)
    {
        return _lps22hh_polling_get_data(sensor, buf);
    }
    else if (sensor->config.mode == RT_SENSOR_MODE_INT)
    {
        return 0;
    }
    else if (sensor->config.mode == RT_SENSOR_MODE_FIFO)
    {
        return _lps22hh_fifo_get_data(sensor, buf, len);
    }
    else
        return 0;
}

static rt_err_t lps22hh_control(struct rt_sensor_device *sensor, int cmd, void *args)
{
    rt_err_t result = RT_EOK;

    switch (cmd)
    {
    case RT_SENSOR_CTRL_GET_ID:
        LPS22HH_ReadID(&lps22hh, args);
        break;
    case RT_SENSOR_CTRL_SET_RANGE:
        result = -RT_ERROR;
        break;
    case RT_SENSOR_CTRL_SET_ODR:
        result = _lps22hh_set_odr(sensor, (rt_uint32_t)args & 0xffff);
        break;
    case RT_SENSOR_CTRL_SET_MODE:
        result = _lps22hh_set_mode(sensor, (rt_uint32_t)args & 0xff);
        break;
    case RT_SENSOR_CTRL_SET_POWER:
        result = _lps22hh_set_power(sensor, (rt_uint32_t)args & 0xff);
        break;
    case RT_SENSOR_CTRL_SELF_TEST:
        break;
    default:
        return -RT_ERROR;
    }
    return result;
}

static struct rt_sensor_ops sensor_ops =
{
    lps22hh_fetch_data,
    lps22hh_control
};

static int rt_hw_lps22hh_init(const char *name, struct rt_sensor_config *cfg)
{
    rt_int8_t result;
    rt_sensor_t sensor_baro = RT_NULL;
    struct rt_sensor_module *module = RT_NULL;
    
    module = rt_calloc(1, sizeof(struct rt_sensor_module));
    if (module == RT_NULL)
    {
        return -1;
    }
    
    {
        sensor_baro = rt_calloc(1, sizeof(struct rt_sensor_device));
        if (sensor_baro == RT_NULL)
            goto __exit;

        sensor_baro->info.type       = RT_SENSOR_CLASS_BARO;
        sensor_baro->info.vendor     = RT_SENSOR_VENDOR_STM;
        sensor_baro->info.model      = "lps22hh_baro";
        sensor_baro->info.unit       = RT_SENSOR_UNIT_MGAUSS;
        sensor_baro->info.intf_type  = RT_SENSOR_INTF_SPI;
        sensor_baro->info.period_min = 100;
        sensor_baro->info.fifo_max   = 32;

        rt_memcpy(&sensor_baro->config, cfg, sizeof(struct rt_sensor_config));
        sensor_baro->ops = &sensor_ops;
        sensor_baro->module = module;
        
        result = rt_hw_sensor_register(sensor_baro, name, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_FIFO_RX, RT_NULL);
        if (result != RT_EOK)
        {
            LOG_E("device register err code: %d", result);
            goto __exit;
        }
    }

    module->sen[0] = sensor_baro;
    module->sen_num = 1;

    if(_lps22hh_baro_init(&cfg->intf) != RT_EOK)
    {
        LOG_E("sensor init failed");
        goto __exit;
    }

    LPS22HH_FIFO_Set_Mode(&lps22hh, LPS22HH_FIFO_BYPASS_MODE);

    return RT_EOK;
    
__exit:
    if(sensor_baro)
    {
        rt_device_unregister(&sensor_baro->parent);
        rt_free(sensor_baro);
    }

    if (module)
        rt_free(module);

    return -RT_ERROR;
}

static int rt_spi2_init(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    rt_hw_spi_device_attach("spi2", LPS22HH_DEVICE_NAME, GPIOA, GPIO_PIN_8);

    spi_dev = (struct rt_spi_bus *)rt_device_find(LPS22HH_DEVICE_NAME);
    if (!spi_dev)
    {
        LOG_E("rt_device_find %s failed!", LPS22HH_DEVICE_NAME);
        return RT_ERROR;
    }

    /* use PA8 as CS */
    rt_pin_mode(LPS22HH_CS_PIN, PIN_MODE_OUTPUT);    /* Set CS */

    struct rt_spi_configuration cfg;
    cfg.data_width = 8;
    cfg.mode = RT_SPI_MASTER | RT_SPI_MODE_0 | RT_SPI_MODE_3; /* SPI Compatible: Mode 0 and Mode 3 */
    cfg.max_hz = 50 * 1000 * 1000; /* 50M */
    rt_spi_configure(spi_dev, &cfg);

    return RT_EOK;
}
INIT_COMPONENT_EXPORT(rt_spi2_init);

int press_sensor_init(void)
{
    struct rt_sensor_config cfg;
    int result;

    cfg.intf.dev_name = LPS22HH_DEVICE_NAME;
    cfg.irq_pin.pin = RT_PIN_NONE;

    result = rt_hw_lps22hh_init(LPS22HH_DEVICE_NAME, &cfg);
    if (RT_EOK != result)
    {
        LOG_E("rt_hw_lps22hh_init failed! result is %d", result);
        return RT_ERROR;
    }

    return RT_EOK;
}


