/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-30     Algo         the first version
 */

#ifndef __SETTING_H__
#define __SETTING_H__

#include <rtthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct setting_info {
    rt_int32_t data_save;
    rt_int32_t auto_screenoff;
}setting_info_t;

void setting_init(void);
int setting_set_info(setting_info_t *info);
setting_info_t *setting_get_info(void);

#ifdef  __cplusplus
}
#endif

#endif /* __SETTING_H */
