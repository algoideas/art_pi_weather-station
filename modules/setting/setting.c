/*
 * Copyright (c) 2020-2030, UNIONMAN
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-21     Algo         the first version
 */

#include <rtthread.h>
#include <easyflash.h>
#include <fal.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DBG_TAG "setting"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#include "setting.h"

#ifdef __cplusplus
extern "C" {
#endif

static setting_info_t s_setting_info = {1, 1};

static int setting_reset_default(void)
{
    char buff[32];
    int len = 0;

    //data_save:1;auto_screenoff:1
    snprintf(buff, 32, "%s", "1:1");
    len = strlen(buff);

    /* set and store the setting config lengths to Env */
    ef_set_env_blob("setting_cfg_len", &len, sizeof(len));

    /* set and store the setting config information to Env */
    ef_set_env_blob("setting_cfg_info", buff, len);

    return len;
}

static int setting_read_cfg(void *buff, int len)
{
    size_t saved_len;

    ef_get_env_blob("setting_cfg_info", buff, len, &saved_len);
    if (saved_len == 0)
    {
        return 0;
    }

    return len;
}

static int setting_get_cfg_len(void)
{
    int len;
    size_t saved_len;

    ef_get_env_blob("setting_cfg_len", &len, sizeof(len), &saved_len);
    if (saved_len == 0)
    {
        return 0;
    }

    return len;
}

static int setting_write_cfg(void *buff, int len)
{
    /* set and store the setting config lengths to Env */
    ef_set_env_blob("setting_cfg_len", &len, sizeof(len));

    /* set and store the setting config information to Env */
    ef_set_env_blob("setting_cfg_info", buff, len);

    return len;
}

void setting_init(void)
{
    char buff[32] = {0};

    if (0 == setting_get_cfg_len())
    {
        setting_reset_default();
        s_setting_info.data_save = 1;
        s_setting_info.auto_screenoff = 1;
    }
    else
    {
        if (0 != setting_read_cfg(buff, sizeof(buff)))
        {
            sscanf(buff, "%d:%d", &s_setting_info.data_save,
                    &s_setting_info.auto_screenoff);
        }
    }
}

int setting_set_info(setting_info_t *info)
{
    char buff[32] = {0};
    int len = 0;

    s_setting_info.data_save = info->data_save;
    s_setting_info.auto_screenoff = info->auto_screenoff;

    snprintf(buff, 32, "%d:%d", info->data_save, info->auto_screenoff);
    len = strlen(buff);

    setting_write_cfg(buff, len);

    return 0;
}

setting_info_t *setting_get_info(void)
{
    return &s_setting_info;
}

#ifdef  __cplusplus
}
#endif
