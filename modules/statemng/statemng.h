/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo       the first version
 */
#ifndef MODULES_STATEMNG_STATEMNG_H_
#define MODULES_STATEMNG_STATEMNG_H_

void statemng_init(void);

#endif /* MODULES_STATEMNG_STATEMNG_H_ */
