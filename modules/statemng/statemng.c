/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-11-01     Algo       the first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <lcd_port.h>

#include "appcomm.h"
#include "eventhub.h"
#include "keymng.h"

#define DBG_COLOR
#define DBG_TAG "STATEMNG"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

#define LED_PIN     GET_PIN(I, 8)

static eventhub_subscriber_t subscriber;

static rt_int32_t statemng_subcriber_callback(eventhub_event_t *event, void *argv)
{
    if (KEY_EVT_DOWN == event->eventId)
    {
        LOG_W("KEY_EVT_DOWN");

        if  (1 == rt_pin_read(LCD_BL_GPIO_NUM))
        {
            rt_pin_write(LCD_BL_GPIO_NUM, 0);
        }
        else
        {
            rt_pin_write(LCD_BL_GPIO_NUM, 1);
        }
    }
    else if (KEY_EVT_HOLD == event->eventId)
    {
        LOG_W("KEY_EVT_HOLD");
    }
    else if (KEY_EVT_UP == event->eventId)
    {
        LOG_W("KEY_EVT_UP");
    }

    return RT_EOK;
}

void statemng_init(void)
{
    rt_uint32_t subscriberHdl;
    int result;

    rt_pin_mode(LED_PIN, PIN_MODE_OUTPUT);

    result = eventhub_init();
    if (RT_EOK != result)
    {
        LOG_E("eventhub_init failed!\n");
        return;
    }

    /* Register KEY EVT */
    eventhub_register(KEY_EVT_DOWN);
    eventhub_register(KEY_EVT_HOLD);
    eventhub_register(KEY_EVT_UP);

    subscriber.subscriberCb = statemng_subcriber_callback;
    subscriber.argv = &subscriber;
    eventhub_create_subscriber(&subscriber, &subscriberHdl);
    eventhub_subscribe(subscriberHdl, KEY_EVT_DOWN);
    eventhub_subscribe(subscriberHdl, KEY_EVT_HOLD);
    eventhub_subscribe(subscriberHdl, KEY_EVT_UP);

    return ;
}
