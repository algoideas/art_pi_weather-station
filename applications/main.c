/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-09-02     RT-Thread    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "drv_common.h"
#include <netdev_ipaddr.h>
#include <netdev.h>
#include <dfs_fs.h>

#include "temp_sensor.h"
#include "press_sensor.h"
#include "gpsmng.h"
#include "sysmng.h"
#include "wifimng.h"
#include "bluetoothmng.h"
#include "netctrl.h"
#include "web.h"
#include "keymng.h"
#include "setting.h"
#include "statemng.h"

#define DBG_COLOR
#define DBG_TAG "main"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>

int main(void)
{
    sysmng_init();
    keymng_init();
    statemng_init();
    temp_sensor_init();

    wifimng_init();
    netctrl_init();
    web_init();
    setting_init();
    bluetoothmng_init();

    return RT_EOK;
}

#include "stm32h7xx.h"
static int vtor_config(void)
{
    /* Vector Table Relocation in Internal QSPI_FLASH */
    SCB->VTOR = QSPI_BASE;
    return 0;
}
INIT_BOARD_EXPORT(vtor_config);
