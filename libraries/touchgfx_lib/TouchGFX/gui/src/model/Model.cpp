#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

#ifndef SIMULATOR
#include <rtthread.h>
#include <temp_sensor.h>
#include <http_client.h>

temp_sensor_info_t *sensor_info;
http_weather_info_t *weather_info;

#endif

Model::Model() : modelListener(0),
    selectedMenuIndex(1)
{

}

void Model::tick()
{
#ifndef SIMULATOR
    sensor_info = temp_sensor_info();
    if (sensor_info != RT_NULL)
    {
        modelListener->updateTempSensorInfo(sensor_info);
    }

    weather_info = http_weather_info();
    if(weather_info != RT_NULL)
    {
        modelListener->updateWeatherInfo(weather_info);
    }
#endif
}
