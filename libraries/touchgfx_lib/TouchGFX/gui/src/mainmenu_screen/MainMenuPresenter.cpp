#include <gui/mainmenu_screen/MainMenuView.hpp>
#include <gui/mainmenu_screen/MainMenuPresenter.hpp>

MainMenuPresenter::MainMenuPresenter(MainMenuView& v)
    : view(v)
{

}

void MainMenuPresenter::activate()
{
    view.setSelectedMenuIndex(model->getSelectedMenuIndex());
}

void MainMenuPresenter::deactivate()
{

}
void MainMenuPresenter::menuSelected(ScreenID id, uint8_t menuIndex)
{
    model->setSelectedMenuIndex(menuIndex);
}

#ifndef SIMULATOR
void MainMenuPresenter::updateTempSensorInfo(temp_sensor_info_t *sensor_info)
{
	view.updateTempSensorInfo(sensor_info);
}

void MainMenuPresenter::updateWeatherInfo(http_weather_info_t *weather_info)
{
	view.updateWeatherInfo(weather_info);
}

void MainMenuPresenter::setDataStorageSave()
{
    view.setDataStorageSave();
}

void MainMenuPresenter::setScreenAutoOff()
{
    view.setScreenAutoOff();
}

void MainMenuPresenter::updateAnalysisGraph()
{
    view.updateAnalysisGraph();
}
#endif
