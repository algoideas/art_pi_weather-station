#include <gui/mainmenu_screen/MainMenuView.hpp>
#include <touchgfx/Color.hpp>
#include "BitmapDatabase.hpp"

#ifndef SIMULATOR
#include "Thread.h"
#include <stdio.h>
#include <rtdevice.h>
#include <board.h>
#include <setting.h>
#include <datastorage.h>
#include <lcd_port.h>

static setting_info_t *setting_info;
static data_info_t info;
#endif

MainMenuView::MainMenuView(): tickCounter(0)
{
}

MainMenuView::~MainMenuView()
{
}

void MainMenuView::setupScreen()
{
    digitalHours = mainDigitalClock.getCurrentHour();
    digitalMinutes = mainDigitalClock.getCurrentMinute();
    digitalSeconds = mainDigitalClock.getCurrentSecond();

    tempMax = -1;

    menuContainer.setXY(0, 0);
    menuContainer.setSwipeCutoff(50);
    menuContainer.setEndSwipeElasticWidth(50);
    menuContainer.setPageIndicatorBitmaps(Bitmap(BITMAP_SCREEN_SWIPE_DOTS_INACTIVE_ID), Bitmap(BITMAP_SCREEN_SWIPE_DOTS_ACTIVE_ID));
    menuContainer.setPageIndicatorXYWithCenteredX(HAL::DISPLAY_WIDTH / 2, HAL::DISPLAY_HEIGHT - 16);

    menuScreenIndex[0] = MainMenuPresenter::SETTING_SCREEN;
    menuScreenIndex[1] = MainMenuPresenter::MAIN_SCREEN;
    menuScreenIndex[2] = MainMenuPresenter::ANALYSIS_SCREEN;

#ifndef SIMULATOR
    setting_info = setting_get_info();
    settingDataSaveButton.forceState(setting_info->data_save);
    settingAutoScreenOffButton.forceState(setting_info->auto_screenoff);
#endif
}

void MainMenuView::afterTransition()
{
    menuContainer.setTouchable(true);
}

void MainMenuView::tearDownScreen()
{
    uint8_t menuIndex = menuContainer.getNumberOfPages();
    presenter->menuSelected(menuScreenIndex[menuIndex], menuIndex);
}

void MainMenuView::setSelectedMenuIndex(uint8_t menuIndex)
{
    menuContainer.setSelectedPage(menuIndex);
}

#ifndef SIMULATOR
void MainMenuView::setDataStorageSave()
{
    setting_info = setting_get_info();
    setting_info->data_save = !setting_info->data_save;
    setting_set_info(setting_info);
}

void MainMenuView::setScreenAutoOff()
{
    setting_info = setting_get_info();
    setting_info->auto_screenoff = !setting_info->auto_screenoff;
    setting_set_info(setting_info);

    /* Reset Auto ScreenOff Tick */
    tickCounter = 1;
}
#endif //END SIMULATOR

#ifndef SIMULATOR
void MainMenuView::updateTempSensorInfo(temp_sensor_info_t *sensor_info)
{
    Unicode::snprintf(mainTemperatureTextBuffer, MAINTEMPERATURETEXT_SIZE, "%d", sensor_info->temp);
    mainTemperatureText.invalidate();

    Unicode::snprintf(mainHumidityTextBuffer, MAINHUMIDITYTEXT_SIZE, "%d", sensor_info->humi);
    mainHumidityText.invalidate();

    /* 温度数据每一分钟存储一次，且在网络校时好的情况下存储
     * 后续加入GPS自动校时功能，不依赖网络
     * */
    if ((lastMinutes != digitalMinutes) && (0 != digitalMon) && (1 == setting_info->data_save))
    {
        lastMinutes = digitalMinutes;
        info.yy = 2000 + (digitalYear - 100);
        info.mm = digitalMon;
        info.dd = digitalDay;
        info.hh = digitalHours;
        info.min = digitalMinutes;
        info.sec = digitalSeconds;
        info.temp = sensor_info->temp;
        info.hum = sensor_info->humi;

        snprintf(fileName, 32, "/sdcard/STG_%d-%02d-%02d.LOG", info.yy, info.mm, info.dd);

        if ((0 != info.temp) && (0 != info.hum))
        {
            if (info.temp > tempMax)
            {
                tempMax = info.temp;
                analysisTextTips.setAlpha(0);
            }

            analysisGraph.addDataPoint(info.temp);
            datastorage_save(fileName, info);
        }
    }
}

/* 更新天气数据，默认更新最近三天的天气信息  */
void MainMenuView::updateWeatherInfo(http_weather_info_t *weather_info)
{
    Unicode::strncpy(mainTodayTempBuffer1, weather_info[0].low, Unicode::strlen(weather_info[0].low));
    Unicode::strncpy(mainTodayTempBuffer2, weather_info[0].high, Unicode::strlen(weather_info[0].high));
    mainTodayTemp.invalidate();

    Unicode::strncpy(mainTomTempBuffer1, weather_info[1].low, Unicode::strlen(weather_info[1].low));
    Unicode::strncpy(mainTomTempBuffer2, weather_info[1].high, Unicode::strlen(weather_info[1].high));
    mainTomTemp.invalidate();

    Unicode::strncpy(mainAfterTomTempBuffer1, weather_info[2].low, Unicode::strlen(weather_info[2].low));
    Unicode::strncpy(mainAfterTomTempBuffer2, weather_info[2].high, Unicode::strlen(weather_info[2].high));
    mainAfterTomTemp.invalidate();

    Unicode::strncpy(mainTodayHumidityBuffer, weather_info[0].humidity, Unicode::strlen(weather_info[0].humidity));
    mainTodayHumidity.invalidate();

    Unicode::strncpy(mainTomHumidityBuffer, weather_info[1].humidity, Unicode::strlen(weather_info[1].humidity));
    mainTomHumidity.invalidate();

    Unicode::strncpy(mainAfterTomHumidityBuffer, weather_info[2].humidity, Unicode::strlen(weather_info[2].humidity));
    mainAfterTomHumidity.invalidate();

    /* 天气现象代码参考： https://docs.seniverse.com/api/start/code.html
          *  目前：仅处理白天
     */
    switch (atoi(weather_info[0].code_day))
    {
        case 0:
        case 1:
        case 2:
        case 3:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_QING_ID));
            break;
        }
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_DUOYUN_ID));
            break;
        }
        case 9:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YIN_ID));
            break;
        }
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YU_ID));
            break;
        }
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_XUE_ID));
            break;
        }
        case 30:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_WU_ID));
            break;
        }
        case 31:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_MAI_ID));
            break;
        }
        default:
        {
            mainImgToday.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_UNKNOWN_ID));
            break;
        }
    }

    mainImgToday.invalidate();

    switch (atoi(weather_info[1].code_day))
    {
        case 0:
        case 1:
        case 2:
        case 3:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_QING_ID));
            break;
        }
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_DUOYUN_ID));
            break;
        }
        case 9:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YIN_ID));
            break;
        }
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YU_ID));
            break;
        }
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_XUE_ID));
            break;
        }
        case 30:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_WU_ID));
            break;
        }
        case 31:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_MAI_ID));
            break;
        }
        default:
        {
            mainImgTomorrow.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_UNKNOWN_ID));
            break;
        }
    }

    mainImgTomorrow.invalidate();

    switch (atoi(weather_info[2].code_day))
    {
        case 0:
        case 1:
        case 2:
        case 3:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_QING_ID));
            break;
        }
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_DUOYUN_ID));
            break;
        }
        case 9:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YIN_ID));
            break;
        }
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_YU_ID));
            break;
        }
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_XUE_ID));
            break;
        }
        case 30:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_WU_ID));
            break;
        }
        case 31:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_MAI_ID));
            break;
        }
        default:
        {
            mainImgAfterTom.setBitmap(touchgfx::Bitmap(BITMAP_WEATHER_UNKNOWN_ID));
            break;
        }
    }

    mainImgAfterTom.invalidate();
}
#endif

void MainMenuView::handleTickEvent()
{
#ifdef SIMULATOR
    tickCounter++;

    if (tickCounter % 60 == 0)
    {
        if (++digitalSeconds >= 60)
        {
            digitalSeconds = 0;
            if (++digitalMinutes >= 60)
            {
                digitalMinutes = 0;
                if (++digitalHours >= 24)
                {
                    digitalHours = 0;
                }
            }
        }

        // Update the digital clock
        mainDigitalClock.setTime24Hour(digitalHours, digitalMinutes, digitalSeconds);
    }
#else
    time_t tNow;
    struct tm *pstTime;

    tickCounter++;

    if ((1 == setting_info->auto_screenoff) && ((tickCounter % 600) == 0))
    {
        if (1 == rt_pin_read(LCD_BL_GPIO_NUM))
        {
            rt_pin_write(LCD_BL_GPIO_NUM, 0);
        }

        tickCounter = 0;
    }

    tNow = time(RT_NULL);
    pstTime = gmtime(&tNow);

    digitalHours = pstTime->tm_hour;
    digitalMinutes = pstTime->tm_min;
    digitalSeconds = pstTime->tm_sec;
    digitalYear = pstTime->tm_year;
    digitalMon = pstTime->tm_mon + 1;
    digitalDay = pstTime->tm_mday;
    digitalWday = pstTime->tm_wday;

    mainDigitalClock.setTime24Hour(digitalHours, digitalMinutes, digitalSeconds);

    memset(mainDateTextBuffer, 0x0, MAINDATETEXT_SIZE);
    Unicode::snprintf(mainDateTextBuffer, MAINDATETEXT_SIZE, "20%d-%d-%d",
            (digitalYear >= 120) ? (digitalYear - 100) : 20, digitalMon, digitalDay);
    mainDateText.invalidate();

    Unicode::snprintf(analysisTempMaxBuffer, ANALYSISTEMPMAX_SIZE, "%d", tempMax);
    analysisTempMax.invalidate();

    switch (digitalWday)
    {
        case 0:
            Unicode::strncpy(mainWeekTextBuffer, "Sun", 4);
            break;
        case 1:
            Unicode::strncpy(mainWeekTextBuffer, "Mon", 4);
            break;
        case 2:
            Unicode::strncpy(mainWeekTextBuffer, "Tue", 4);
            break;
        case 3:
            Unicode::strncpy(mainWeekTextBuffer, "Wed", 4);
            break;
        case 4:
            Unicode::strncpy(mainWeekTextBuffer, "Thu", 4);
            break;
        case 5:
            Unicode::strncpy(mainWeekTextBuffer, "Fri", 4);
            break;
        case 6:
            Unicode::strncpy(mainWeekTextBuffer, "Sat", 4);
            break;
        default:
            Unicode::strncpy(mainWeekTextBuffer, "Mon", 4);
            break;
    }
    mainWeekText.invalidate();
#endif //END SIMULATOR
}

#ifndef SIMULATOR
void MainMenuView::updateAnalysisGraph()
{
    //Need Update
}
#endif
