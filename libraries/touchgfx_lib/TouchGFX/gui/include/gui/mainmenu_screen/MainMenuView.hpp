#ifndef MAINMENUVIEW_HPP
#define MAINMENUVIEW_HPP

#include <gui_generated/mainmenu_screen/MainMenuViewBase.hpp>
#include <gui/mainmenu_screen/MainMenuPresenter.hpp>
#include <mvp/View.hpp>
#include <touchgfx/widgets/Box.hpp>
#include <touchgfx/containers/SwipeContainer.hpp>
#include <BitmapDatabase.hpp>
#include <touchgfx/containers/Container.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/graph/GraphScroll.hpp>
#include <touchgfx/widgets/graph/GraphElements.hpp>

#ifndef SIMULATOR
#include <temp_sensor.h>
#include <http_client.h>
#include <setting.h>
#endif

class MainMenuView : public MainMenuViewBase
{
public:
    MainMenuView();
    virtual ~MainMenuView();
    virtual void setupScreen();
    virtual void tearDownScreen();
    void setSelectedMenuIndex(uint8_t menuIndex);

    virtual void afterTransition();
	virtual void handleTickEvent();

#ifndef SIMULATOR
	virtual void updateTempSensorInfo(temp_sensor_info_t *sensor_info);
	virtual void updateWeatherInfo(http_weather_info_t *weather_info);
    virtual void setDataStorageSave();
    virtual void setScreenAutoOff();
    virtual void updateAnalysisGraph();
#endif

protected:

    static const int NUMBER_OF_MENU_SCREENS = 3;

    MainMenuPresenter::ScreenID menuScreenIndex[NUMBER_OF_MENU_SCREENS];

#ifdef SIMULATOR
    touchgfx::MoveAnimator< touchgfx::SwipeContainer > menuContainer;
    touchgfx::Container settingScreenContainer;
    touchgfx::Container mainScreenContainer;
    touchgfx::Image mainBackground;
    touchgfx::DigitalClock mainDigitalClock;
    touchgfx::TextArea mainWeekText;
    touchgfx::TextArea mainDateText;
    touchgfx::Image mainImgAfterTom;
    touchgfx::Image mainImgTomorrow;
    touchgfx::Image mainImgToday;
    touchgfx::Container analysisScreenContainer;
#endif

    static const int FILENAME_MAX_SIZE = 32;
    char fileName[FILENAME_MAX_SIZE];
    int tempMax;

    int lastMinutes;
	int tickCounter;
    int digitalHours;
    int digitalMinutes;
    int digitalSeconds;
    int digitalYear;
    int digitalMon;
    int digitalDay;
	int digitalWday;
};

#endif // MAINMENUVIEW_HPP
