#ifndef MAINMENUPRESENTER_HPP
#define MAINMENUPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

#ifndef SIMULATOR
#include <temp_sensor.h>
#include <http_client.h>
#endif

using namespace touchgfx;

class MainMenuView;

class MainMenuPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    enum ScreenID
    {
        SETTING_SCREEN = 0,
        MAIN_SCREEN,
        ANALYSIS_SCREEN,
        NO_MENU_SCREEN
    };
    MainMenuPresenter(MainMenuView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~MainMenuPresenter() {};

#ifndef SIMULATOR
	virtual void updateTempSensorInfo(temp_sensor_info_t *sensor_info);
	virtual void updateWeatherInfo(http_weather_info_t *weather_info);
    virtual void setDataStorageSave();
    virtual void setScreenAutoOff();
    virtual void updateAnalysisGraph();
#endif

    void menuSelected(ScreenID id, uint8_t menuIndex);
private:
    MainMenuPresenter();

    MainMenuView& view;
};

#endif // MAINMENUPRESENTER_HPP
