#ifndef MODELLISTENER_HPP
#define MODELLISTENER_HPP

#include <gui/model/Model.hpp>

#ifndef SIMULATOR
#include <temp_sensor.h>
#include <http_client.h>
#endif


class ModelListener
{
public:
    ModelListener() : model(0) {}
    
    virtual ~ModelListener() {}

    void bind(Model* m)
    {
        model = m;
    }

#ifndef SIMULATOR
    virtual void updateTempSensorInfo(temp_sensor_info_t *sensor_info)
    {
        // Override and implement this function in Presenter
    }
    virtual void updateWeatherInfo(http_weather_info_t *weather_info)
    {
        // Override and implement this function in Presenter
    }
#endif

protected:
    Model* model;
};

#endif // MODELLISTENER_HPP
