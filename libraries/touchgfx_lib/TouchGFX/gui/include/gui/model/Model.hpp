#ifndef MODEL_HPP
#define MODEL_HPP

#include <touchgfx/Utils.hpp>
class ModelListener;

class Model
{
public:
    Model();

    void bind(ModelListener* listener)
    {
        modelListener = listener;
    }

    void tick();
    void setSelectedMenuIndex(uint8_t index)
    {
        selectedMenuIndex = index;
    }

    uint8_t getSelectedMenuIndex() const
    {
        return selectedMenuIndex;
    }
protected:
    ModelListener* modelListener;
    uint8_t selectedMenuIndex;
};

#endif // MODEL_HPP
