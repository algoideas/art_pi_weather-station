/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#ifndef TEXTKEYSANDLANGUAGES_HPP
#define TEXTKEYSANDLANGUAGES_HPP


typedef enum
{
    GBR,
    NUMBER_OF_LANGUAGES
} LANGUAGES;


typedef enum
{
    T_SINGLEUSEID1,
    T_SINGLEUSEID2,
    T_SINGLEUSEID4,
    T_SINGLEUSEID7,
    T_SINGLEUSEID8,
    T_SINGLEUSEID9,
    T_SINGLEUSEID10,
    T_SINGLEUSEID11,
    T_SINGLEUSEID12,
    T_SINGLEUSEID13,
    T_SINGLEUSEID14,
    T_SINGLEUSEID15,
    T_SINGLEUSEID16,
    T_SINGLEUSEID18,
    T_SINGLEUSEID20,
    T_SINGLEUSEID22,
    T_SINGLEUSEID23,
    T_SINGLEUSEID24,
    T_SINGLEUSEID25,
    T_SINGLEUSEID26,
    T_SINGLEUSEID27,
    T_SINGLEUSEID28,
    T_SINGLEUSEID29,
    T_SINGLEUSEID30,
    T_ANALYSIS_TEMP,
    T_DATA_SAVE,
    T_GPS_TIME,
    T_SCREEN_AUTOOFF,
    T_SINGLEUSEID31,
    T_SINGLEUSEID32,
    T_ANALYSI_NODATA,
    T_SINGLEUSEID33,
    T_SINGLEUSEID34,
    T_SINGLEUSEID35,
    NUMBER_OF_TEXT_KEYS
} TEXTS;

#endif // TEXTKEYSANDLANGUAGES_HPP
