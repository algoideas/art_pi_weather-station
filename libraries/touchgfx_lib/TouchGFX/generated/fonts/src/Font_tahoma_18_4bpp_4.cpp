#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_tahoma_18_4bpp_4[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x2103]
    0x00, 0x30, 0x14, 0x00, 0x00, 0x00, 0x30, 0x45, 0x01, 0x00, 0x10, 0xFC, 0xEF, 0x06, 0x00, 0x70,
    0xFE, 0xFF, 0xAF, 0x03, 0xA0, 0x3D, 0x81, 0x3F, 0x00, 0xF9, 0x5C, 0x43, 0xF9, 0x08, 0xF0, 0x06,
    0x00, 0x7E, 0x50, 0xBF, 0x00, 0x00, 0x30, 0x06, 0xF1, 0x05, 0x00, 0x7D, 0xC0, 0x2F, 0x00, 0x00,
    0x00, 0x00, 0xB0, 0x2D, 0x80, 0x3F, 0xF1, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x20, 0xFC, 0xFF, 0x06,
    0xF3, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x25, 0x00, 0xF3, 0x09, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xF2, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x0D,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB0, 0x4F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x40, 0xDF, 0x01, 0x00, 0x50, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x8E, 0x76,
    0xFC, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x50, 0xFC, 0xFF, 0x7D, 0x01, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x10, 0x12, 0x00, 0x00
};
