### prebuilt目录说明

主要包含镜像 (可以快速验证界面功能)：
1、rtthread.elf  - 应用程序,已经编译好的RT_Thread应用程序
2、images.bin    - TouchGFX图片相关资源,GUI需要用到,请放到SD卡根目录

注意：
1、DHT11默认连接在ART-Pi的PI5，请参考modules\hal\tempsensor\temp_sensor.h定义自行修改;
2、默认采用的屏幕是4.3英寸的正点原子触摸屏,触摸芯片GT9147;
3、联网更新数据，请先采用ART-Pi自带的配网功能进行配置，具体请参考SDK相关说明：UM5001-RT-Thread ART-Pi 快速上手.md 
   及其他相关资料。