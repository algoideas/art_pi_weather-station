# ART_Pi_WeatherStation

#### 介绍
ART-Pi Weather Station （智能天气小站）

#### 软件架构
软件架构说明

项目主要基于ART-Pi SDK中的art_pi_factory例程，并在例程基础上增加其他软件基础组件。内核采用RT-Thread原生内核，版本RT-Thread v4.0.3

组件列表：
    rt_ota_lib – 升级相关
    touchgfx_lib – TouchGFX相关界面实现
    wlan_wiced_lib – WLAN库
    涉及软件包：
    EasyFlash-v4.1.0  -- 轻量级物联网存储库
    adbd-v1.1.1 – ADB远程调试
    btstack-v0.0.1 – 蓝牙协议栈
    cJSON-v1.0.2 – JSON库
    dht11-latest – DHT11温度传感器库
    fal-v0.5.0 - 
    gt9147-latest  -- 触摸驱动
    littlefs-v2.0.5 --
    lwgps-latest  -- 轻量级GPS解析库
    netutils-v1.2.0 – 网络相关工具
    webclient-v2.1.2 – WEB客户端
    webnet-v2.0.2 – web接口相关实现

    注：gps和presssensor相关包或模块可供二次开发

 **硬件框架** 
    主控芯片：STM32H750XB （ART-Pi开发板）
    图形框架：TouchGFX 
    SD卡1张: 8G
    温湿度传感器DHT11：使用GPIO获取数据
    网络：板端自带的AP6212A WIFI+蓝牙一体芯片
    4.3寸电容屏：使用I2C总线驱动触摸（触摸芯片GT9147),LCD使用RGB888格式
    时间的获取：例程自带的RTC+NTP自动网络对时
    天气的获取：通过http客户端的方式获取网络开放API接口的天气数据，目前主要从心知天气（https://www.seniverse.com/api）获取，
        后续二次开发，也可采用其他网站获取，如openweathermap(https://api.openweathermap.org/data/2.5/)等
    
 **软件框架说明** 
    软件如果需要通过网络获取天气信息，需要先进行联网，目前支持蓝牙配网（具体请参考ART-Pi官方教程），目前仅支持本地预先设置好的地区的温度的获取，后续可以扩展UI设计，或通过GPS定位来获取位置信息。
    同时，默认支持DHT11数字温度传感器获取本地的实时温湿度数据，并通过GUI进行显示。软件主体框架如下图所示：
    系统设置界面，支持将温湿度数据存储为日志文件到SD卡（后续可以二次开发，读取历史文件并直接在UI界面显示历史数据），可以开启或关闭该功能，同时支持自动关屏设置。
    ![主页](https://images.gitee.com/uploads/images/2020/1217/191139_a0f1eff8_1043810.jpeg "Home.jpg")
    ![系统设置](https://images.gitee.com/uploads/images/2020/1217/191213_25998e1e_1043810.jpeg "Setting.jpg")
    ![历史数据](https://images.gitee.com/uploads/images/2020/1217/191231_cf524e00_1043810.jpeg "Analysis.jpg")

    视频演示地址：
    [视频演示](https://www.bilibili.com/video/BV1tv411476Y)

 **软件模块说明** 
UI设计
    GUI采用TouchGFX来设计，目前采用的版本是TouchGFX 4.15.0，主要分为3个主要界面：主页、系统设置和历史温度数据分析，分别如下图图1、图2和图3所示。
    TouchGFX官方介绍：
    TouchGFX 4.13 版本是继TouchGFX 4.12 之后的又一重要版本。4.13版具备了将动画推到60FPS的功能，还增加了可缓存容器、不完全帧缓冲区以及新的L8压缩格式等性能，这表明了 ToughGFX 不断追求优化性能和持续迭代的匠心。4.13版本还解决了另一个问题：嵌入式系统开发人员的用户界面可访问性。通过将TouchGFX Generator集成到 STM32CubeMX 中，经验较少的工程师在使用 TouchGFX 4.13 时，可以通过 STM32CubeMX 这个广为流行的 ST 实用软件程序快速启动项目。这样做的目的在于降低开发者入门门槛，让专业人士和爱好者都能受益于这个交互式的高效解决方案。

天气数据获取
    1、通过DHT11数字温湿度传感器获取；
    2、通过注册心知天气（https://www.seniverse.com/），采用其Weather API，并配合web相关API获取当地的天气；



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
